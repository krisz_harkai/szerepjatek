# **A feladat** #
Szöveges alapú szerepjáték készítése. Hagyományos alapok, véletlenszerű ellenfelek, különböző nehézségek. Esetleg könnyedebb humor.

# **Felületi terv** #
https://app.moqups.com/sicike/u0WeUn9Z3Y/view/page/a0e5ea0ef

# **Felhasználó** #
## 1. Felhasználó regisztrálása, bejelentkezés. ##
* Adatbázisban tároljuk a felhasználó által megadott értékeket, majd ez alapján be tudjuk léptetni.
## 2. Új karakter létrehozása. ##
* Adatbázisban tároljuk a felhasználó által létrehozott karaktereket.
* Több létrehozására is lesz lehetőség.
* Meg kell adnunk a nemét, faját, néhány külső jelzőt, valamint hogy milyen karakterosztályba fog tartozni.
## 3. Üzlet különböző játékon belüli tárgyak vásárlásához. ##
* Játékon belüli fizetőeszközért vásárolhat itt.
* Különböző páncélok, fegyverek, kiegészítők.
* Admin módosíthatja ennek a tartalmát.
## 4. Karakterböngésző, más karaktereinek megtekintése. ##
* Képpel, szinttel megjelenítve.
* Megjegyzés hozzáfűzése más karakterei alá.
## 5. A játék maga.
* Minden egyes játék indítása egy új (random) generált kihívás lesz.
* Egyszerű megjelenítés, középen a szöveg, hogy mi is történik.
* Saját karakterünk bal oldalon, az ellenfél karaktere jobb oldalt.
* harc végén zsákmány, vagy vereség esetén valamilyen szankció bevezetése. 
## 6. Visszajelzés küldése. ##
* A játékról alkotott vélemény, esetleges hibák jelzése.

# **Admin** #
## 1. üzlet tartalmának módosítása ##
* Új tárgyak felvétele, régiek törlése, módosítása.
## 2. Akciók létrehozása ##
* Az üzlet árainak csökkentése bizonyos százalékkal.
## 3. Visszajelzések megtekintése ##
* A felhasználóktól beérkező visszajelzések konstatálása, bizonyos esetekben továbbítása.

# **Táblázat** #
**Felhasználó**   | **Admin**
----------------- | ----------------
Regisztrálás      | Üzlet módosítása
Bejelentkezés     | Akciók
Karakterkészítés  | Visszajelzés megtekintése
Üzlet használata  | 
Más karakterének böngészése | 
Játék | 
Visszajelzés küldése |

# **UML diagram** 
#![UML.jpg](https://bitbucket.org/repo/M8gge8/images/3166908989-UML.jpg)