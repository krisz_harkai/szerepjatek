package szerepjatek;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import szerepjatek.dao.FelhasznaloSzolgDao;
import szerepjatek.dao.KarakterSzolgDao;
import szerepjatek.dao.UzletSzolgDao;
import szerepjatek.dao.impl.JpaFelhasznaloSzolgDao;
import szerepjatek.dao.impl.JpaKarakterSzolgDao;
import szerepjatek.dao.impl.JpaUzletSzolgDao;
import szerepjatek.services.FelhasznaloSzolgaltatasok;
import szerepjatek.services.KarakterSzolgaltatasok;
import szerepjatek.services.UzletSzolgaltatasok;
import szerepjatek.services.impl.FelhasznaloSzolgaltatasokImpl;
import szerepjatek.services.impl.KarakterSzolgaltatasokImpl;
import szerepjatek.services.impl.UzletSzolgaltatasokImpl;

@Configuration
public class SzerepjatekConfig {

	@Bean
	public FelhasznaloSzolgDao felhSzolgDao() {
	return new JpaFelhasznaloSzolgDao();
	}
	
	@Bean
	public KarakterSzolgDao karSzolgDao() {
	return new JpaKarakterSzolgDao();
	}
	
	@Bean
	public UzletSzolgDao uzletSzolgDao() {
	return new JpaUzletSzolgDao();
	}
	
	
	@Bean
	public FelhasznaloSzolgaltatasok felhasznaloSzolgaltatasok(FelhasznaloSzolgDao fszdao){
		return new FelhasznaloSzolgaltatasokImpl(fszdao);
	}
	
	@Bean
	public KarakterSzolgaltatasok KarakterSzolgaltatasokImpl(KarakterSzolgDao kszdao){
		return new KarakterSzolgaltatasokImpl(kszdao);
	}
	
	@Bean
	public UzletSzolgaltatasok UzletSzolgaltatasokImpl(UzletSzolgDao uszdao){
		return new UzletSzolgaltatasokImpl(uszdao);
	}
}
