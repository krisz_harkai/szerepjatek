package szerepjatek.dao;

import szerepjatek.entities.Felhasznalo;

/**
 * A FelhasználóSzolgáltatásokhoz köthető egyéb függvények, amik a GenericDao-ban nem szerepelnek
 * @author Gyakornok
 *
 */
public interface FelhasznaloSzolgDao extends GenericDao {
	/**
	 * Felhasználó azonosítása név és jelszó alapján
	 * @param nev
	 * @param jelszo
	 * @return
	 */
	Felhasznalo findByNameAndPass(String felhNev, String jelszo);
}
