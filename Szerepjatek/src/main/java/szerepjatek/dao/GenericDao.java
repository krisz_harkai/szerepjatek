package szerepjatek.dao;

import java.util.List;

/**
 * Generic for dao classes
 * @author Gyakornok
 *
 * @param <K> key
 * @param <E> entity
 */
public interface GenericDao{
	
	/**
	 * creating a new entity
	 * @param entity 
	 * @return
	 */
	 <E> E create(E entity);
	
	/**
	 * finding an entity by its ID and returning it
	 * @param id
	 * @return
	 */
	 <E, K> E read(Class<E> type, K id);
	
	/**
	 * Deleting an entity
	 * @param entity
	 */
	 <E> void delete(E entity);
	
	/**
	 * updating and entity
	 * @param entity
	 * @return
	 */
	 <E> E update(E entity);
	
	/**
	 * Returns all the entities of the same type
	 * @return
	 */
	 <E> List<E> findAll(Class<E> type);
}
