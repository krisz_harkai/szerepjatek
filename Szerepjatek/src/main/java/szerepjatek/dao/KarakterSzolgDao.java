package szerepjatek.dao;

import java.util.List;

import szerepjatek.entities.Felszereles;
import szerepjatek.entities.Jatek;
import szerepjatek.entities.JatekSztori;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.entities.Tipus;

/**
 * A KarakterSzolgáltatásokhoz köthető egyéb függvények, amik a GenericDao-ban nem szerepelnek
 * @author Gyakornok
 *
 */
public interface KarakterSzolgDao extends GenericDao{
	/**
	 * Egy felszereles felvétele, magára aggatása a játkosnak
	 * Ha már azon a helyen van egy másik olyan típusú, akkor a zsákjába kerül a másik
	 * Közben az értékek (statok) megfelelően változnak
	 *  ----> levonjuk aaz egyezőét, hozzáadjuk az újat
	 * @param jatk a játékos karaktere
	 * @param felsz a felszerelés, amit fel akarunk aggatni magunkra
	 * @return az updatelt játékoskarakter
	 */
	JatekosKarakter addHordva(JatekosKarakter jatk, Felszereles felsz);
	
	/**
	 * Egy felszerelés sima eltávolítása a karakterünkről, ezzel zsákba való helyezése
	 * Ha leszedjük magunkról, akkor az értékek is csökkennek a felszerelés értékeivel
	 * @param jatk a játékos karaktere
	 * @param felsz a felszerelés, amit fel akarunk aggatni magunkra
	 * @return az updatelt játékoskarakter
	 */
	JatekosKarakter removeHordva(JatekosKarakter jatk, Felszereles felsz);
	
	/**
	 * Adott típuson belüli összes felszerelés visszaadása
	 * @param tipus a felszerelés típusa
	 * @return felszerelések egy listája az adott típuson belül
	 */
	List<Felszereles> getAllFelszereles(Tipus tipus);
	
	/**
	 * A JátékSztorik listájából (query-vel megkapva) egy véletlenszerű Sztori kiválasztása
	 * @return a zsír új véletlen történet
	 */
	JatekSztori chooseRandomStory();
}
