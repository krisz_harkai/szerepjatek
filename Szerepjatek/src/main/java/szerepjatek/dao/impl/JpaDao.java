package szerepjatek.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import szerepjatek.dao.GenericDao;

public class JpaDao implements GenericDao {
	
	@PersistenceContext
	protected EntityManager entityManager;
	
	@Override
	public <E> E create(E entity) {
		try {
			this.entityManager.persist(entity);
		} catch (Exception e) {
			return null;
		}
		return entity;
	}

	@Override
	public <E, K> E read(Class<E> type, K id) {
		return this.entityManager.find(type, id);
	}

	@Override
	public <E> void delete(E entity) {
		entity = this.entityManager.merge(entity);
		try {
			this.entityManager.remove(entity);
		} catch (Exception e) {
		}
	}

	@Override
	public <E> E update(E entity) {
		return this.entityManager.merge(entity);
	}

	@Override
	public <E> List<E> findAll(Class<E> type) {
		return entityManager.createQuery("from " + type.getName(), type)
				.getResultList();
	}
	
}
