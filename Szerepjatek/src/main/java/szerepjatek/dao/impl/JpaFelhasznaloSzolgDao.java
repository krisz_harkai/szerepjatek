package szerepjatek.dao.impl;

import java.util.List;

import szerepjatek.dao.FelhasznaloSzolgDao;
import szerepjatek.entities.Felhasznalo;

public class JpaFelhasznaloSzolgDao extends JpaDao implements FelhasznaloSzolgDao{
	@Override
	public Felhasznalo findByNameAndPass(String felhNev, String jelszo) {
		List<Felhasznalo> felhk =
		 entityManager.createQuery("from Felhasznalo f where f.felhNev = :felhNev and f.jelszo = :jelsz", Felhasznalo.class)
       .setParameter("felhNev", felhNev)
       .setParameter("jelsz", jelszo)
       .getResultList();

		return felhk.isEmpty() ? null : felhk.get(0); 
	}
	
}
