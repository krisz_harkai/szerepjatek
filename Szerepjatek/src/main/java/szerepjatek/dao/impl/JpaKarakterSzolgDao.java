package szerepjatek.dao.impl;

import java.util.List;
import java.util.Random;

import szerepjatek.dao.KarakterSzolgDao;
import szerepjatek.entities.Felszereles;
import szerepjatek.entities.Jatek;
import szerepjatek.entities.JatekSztori;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.entities.Tipus;

public class JpaKarakterSzolgDao extends JpaDao implements KarakterSzolgDao {

	@Override
	public JatekosKarakter addHordva(JatekosKarakter jatk, Felszereles felsz) {
		Felszereles eltavolitando = null;
		boolean voltMarRajta = false;
		
		for (Felszereles felssz : jatk.getHordva()) {
			if(felssz.getTipus()==felsz.getTipus()){
				eltavolitando = felssz;
				voltMarRajta=true;
			}
		}
		if(voltMarRajta){
			jatk.getZsak().add(eltavolitando);
			jatk.getHordva().remove(eltavolitando);
			jatk = decreaseStats(jatk,eltavolitando);
		}
		jatk.getHordva().add(felsz);
		jatk.getZsak().remove(felsz);
		jatk = increaseStats(jatk,felsz);
		return update(jatk);
	}

	@Override
	public JatekosKarakter removeHordva(JatekosKarakter jatk, Felszereles felsz) {
		jatk.getHordva().remove(felsz);
		jatk=decreaseStats(jatk,felsz);
		
		return update(jatk);
	}

	@Override
	public JatekSztori chooseRandomStory() {
		Random rand = new Random();
		List<JatekSztori> jszk = entityManager.createQuery("From JatekSztori", JatekSztori.class).getResultList();
		return (jszk.get(rand.nextInt(jszk.size())));
	}

	@Override
	public List<Felszereles> getAllFelszereles(Tipus tipus) {
		return entityManager.createQuery("from Felszereles f where f.tipus = :tipus", Felszereles.class)
		       .setParameter("tipus", tipus)
		       .getResultList();
	}

	private JatekosKarakter increaseStats(JatekosKarakter jatk, Felszereles felsz) {
		jatk.setTamadas(jatk.getTamadas()+felsz.getTamadas());
		jatk.setVedekezes(jatk.getVedekezes()+felsz.getVedekezes());
		jatk.setElet(jatk.getElet()+felsz.getElet());
		jatk.setMaxElet(jatk.getMaxElet()+felsz.getElet());
		jatk.setMana(jatk.getMana()+felsz.getMana());
		jatk.setMaxMana(jatk.getMaxMana()+felsz.getMana());
		return jatk;
	}

	private JatekosKarakter decreaseStats(JatekosKarakter jatk, Felszereles felsz) {
		jatk.setTamadas(jatk.getTamadas()-felsz.getTamadas());
		jatk.setVedekezes(jatk.getVedekezes()-felsz.getVedekezes());
		jatk.setElet(jatk.getElet()-felsz.getElet());
		jatk.setMaxElet(jatk.getMaxElet()-felsz.getElet());
		jatk.setMana(jatk.getMana()-felsz.getMana());
		jatk.setMaxMana(jatk.getMaxMana()-felsz.getMana());
		return jatk;
	}

}
