package szerepjatek.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
public class Felhasznalo {

	@Id
	private String felhNev;
	private String nev;
	private String jelszo;
	private String email;
	
	@Enumerated(EnumType.STRING)
	private Jogkor jogkor;
	
	
	
	@OneToOne(targetEntity=JatekosKarakter.class)
	private JatekosKarakter aktivKarakter;
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="felhasznalo",targetEntity=JatekosKarakter.class,
			cascade = {CascadeType.ALL}, orphanRemoval=true)
	private List<JatekosKarakter> karakterek;
	
	
	
	@OneToMany(fetch = FetchType.EAGER, mappedBy="felhasznalo",targetEntity=Jatek.class,
			cascade = {CascadeType.ALL}, orphanRemoval=true)
	private List<Jatek> jatekok;

	public String getNev() {
		return nev;
	}

	public String getFelhNev() {
		return felhNev;
	}

	public void setFelhNev(String felhNev) {
		this.felhNev = felhNev;
	}

	public String getJelszo() {
		return jelszo;
	}

	public void setJelszo(String jelszo) {
		this.jelszo = jelszo;
	}

	public Jogkor getJogkor() {
		return jogkor;
	}

	public void setJogkor(Jogkor jogkor) {
		this.jogkor = jogkor;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public JatekosKarakter getAktivKarakter() {
		return aktivKarakter;
	}

	public void setAktivKarakter(JatekosKarakter aktivKarakter) {
		this.aktivKarakter = aktivKarakter;
	}

	public List<JatekosKarakter> getKarakterek() {
		return karakterek;
	}

	public void setKarakterek(List<JatekosKarakter> karakterek) {
		this.karakterek = karakterek;
	}

	public List<Jatek> getJatekok() {
		return jatekok;
	}

	public void setJatekok(List<Jatek> jatekok) {
		this.jatekok = jatekok;
	}

	@Override
	public String toString() {
		return "felhNev=" + felhNev;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((felhNev == null) ? 0 : felhNev.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Felhasznalo other = (Felhasznalo) obj;
		if (felhNev == null) {
			if (other.felhNev != null)
				return false;
		} else if (!felhNev.equals(other.felhNev))
			return false;
		return true;
	}
	
	

}
