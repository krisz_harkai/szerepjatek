package szerepjatek.entities;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity
public class Felszereles {
	
	@Id
	private String nev;
	private String leiras;
	@Enumerated(EnumType.STRING)
	private Tipus tipus;
	private int ar;
	@Enumerated(EnumType.STRING)
	private Ritkasag ritkasag;
	
	private int tamadas;
	private int vedekezes;
	private int elet;
	private int mana;

	public int getTamadas() {
		return tamadas;
	}

	public void setTamadas(int tamadas) {
		this.tamadas = tamadas;
	}

	public int getVedekezes() {
		return vedekezes;
	}

	public void setVedekezes(int vedekezes) {
		this.vedekezes = vedekezes;
	}

	public int getElet() {
		return elet;
	}

	public void setElet(int elet) {
		this.elet = elet;
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		this.mana = mana;
	}

	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public String getLeiras() {
		return leiras;
	}

	public void setLeiras(String leiras) {
		this.leiras = leiras;
	}

	public Tipus getTipus() {
		return tipus;
	}

	public void setTipus(Tipus tipus) {
		this.tipus = tipus;
	}

	public int getAr() {
		return ar;
	}

	public void setAr(int ar) {
		this.ar = ar;
	}

	public Ritkasag getRitkasag() {
		return ritkasag;
	}

	public void setRitkasag(Ritkasag ritkasag) {
		this.ritkasag = ritkasag;
	}
	
}
