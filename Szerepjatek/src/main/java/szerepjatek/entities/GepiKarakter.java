package szerepjatek.entities;

import javax.persistence.Entity;

@Entity
public class GepiKarakter extends Karakter {
	private String kezdoSzoveg;
	private String tamadasSzoveg;
	private String halalSzoveg;
	
	public String getKezdoSzoveg() {
		return kezdoSzoveg;
	}
	public void setKezdoSzoveg(String kezdoSzoveg) {
		this.kezdoSzoveg = kezdoSzoveg;
	}
	public String getTamadasSzoveg() {
		return tamadasSzoveg;
	}
	public void setTamadasSzoveg(String tamadasSzoveg) {
		this.tamadasSzoveg = tamadasSzoveg;
	}
	public String getHalalSzoveg() {
		return halalSzoveg;
	}
	public void setHalalSzoveg(String halalSzoveg) {
		this.halalSzoveg = halalSzoveg;
	}

	

}