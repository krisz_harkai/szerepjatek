package szerepjatek.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Jatek {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	@Enumerated(EnumType.STRING)
	private JatekVegek vege;
	
	@ManyToOne(targetEntity=Felhasznalo.class)
	private Felhasznalo felhasznalo;
	@ManyToOne(targetEntity=JatekosKarakter.class)
	private JatekosKarakter karakter;
	@OneToMany(targetEntity=JatekSztori.class)
	private List<JatekSztori> jatekSztorik;
	
	public JatekosKarakter getKarakter() {
		return karakter;
	}

	public void setKarakter(JatekosKarakter karakter) {
		this.karakter = karakter;
	}

	public List<JatekSztori> getJatekSztorik() {
		return jatekSztorik;
	}

	public void setJatekSztorik(List<JatekSztori> jatekSztorik) {
		this.jatekSztorik = jatekSztorik;
	}

	public Felhasznalo getFelhasznalo() {
		return felhasznalo;
	}

	public void setFelhasznalo(Felhasznalo felhasznalo) {
		this.felhasznalo = felhasznalo;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public JatekVegek getVege() {
		return vege;
	}

	public void setVege(JatekVegek vege) {
		this.vege = vege;
	}
}
