package szerepjatek.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class JatekSztori {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String nev;
	private String tortenet;
	
	@OneToOne(targetEntity=GepiKarakter.class)
	private GepiKarakter gepiEllenfel;
	
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public String getTortenet() {
		return tortenet;
	}

	public void setTortenet(String tortenet) {
		this.tortenet = tortenet;
	}

	public GepiKarakter getGepiEllenfel() {
		return gepiEllenfel;
	}

	public void setGepiEllenfel(GepiKarakter gepiEllenfel) {
		this.gepiEllenfel = gepiEllenfel;
	}


}
