package szerepjatek.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.IndexColumn;

@Entity
public class JatekosKarakter extends Karakter {
	private String nem;
	private String faj;
	private int eletkor;
	private long tp; //tapasztalati pont
	private long kovszintTp; //következő szintig mennyi tapasztalati pont kell
	
	@ManyToOne
	private Felhasznalo felhasznalo;
	@OneToOne
	private Osztaly osztaly;
	@ManyToMany(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinTable(name="karakter_horva")
	private List<Felszereles> hordva;
	@ManyToMany(fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@JoinTable(name="karakter_zsak")
	private List<Felszereles> zsak;
	
	public String getNem() {
		return nem;
	}

	public void setNem(String nem) {
		this.nem = nem;
	}

	public String getFaj() {
		return faj;
	}

	public void setFaj(String faj) {
		this.faj = faj;
	}

	public int getEletkor() {
		return eletkor;
	}

	public void setEletkor(int eletkor) {
		this.eletkor = eletkor;
	}

	public Osztaly getOsztaly() {
		return osztaly;
	}

	public void setOsztaly(Osztaly osztaly) {
		this.osztaly = osztaly;
	}
	
	public long getTp() {
		return tp;
	}

	public void setTp(long tp) {
		this.tp = tp;
	}

	public long getKovszintTp() {
		return kovszintTp;
	}

	public void setKovszintTp(long kovszintTp) {
		this.kovszintTp = kovszintTp;
	}

	public Felhasznalo getFelhasznalo() {
		return felhasznalo;
	}

	public void setFelhasznalo(Felhasznalo felhasznalo) {
		this.felhasznalo = felhasznalo;
	}

	public List<Felszereles> getHordva() {
		return hordva;
	}

	public void setHordva(List<Felszereles> hordva) {
		this.hordva = hordva;
	}

	public List<Felszereles> getZsak() {
		return zsak;
	}

	public void setZsak(List<Felszereles> zsak) {
		this.zsak = zsak;
	}

	@Override
	public String toString() {
		return "nev="+super.getNev();
	}

	
}
