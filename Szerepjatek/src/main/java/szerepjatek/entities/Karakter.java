package szerepjatek.entities;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class Karakter {
	
	@Id
	private String nev;
	private int szint;
	private long arany;
	private int tamadas;
	private int vedekezes;
	private int maxElet;
	private int elet;
	private int maxMana;
	private int mana;

	public int getTamadas() {
		return tamadas;
	}

	public void setTamadas(int tamadas) {
		this.tamadas = tamadas;
	}

	public int getVedekezes() {
		return vedekezes;
	}

	public void setVedekezes(int vedekezes) {
		this.vedekezes = vedekezes;
	}

	public int getElet() {
		return elet;
	}

	public void setElet(int elet) {
		this.elet = elet;
	}

	public int getMana() {
		return mana;
	}

	public void setMana(int mana) {
		this.mana = mana;
	}

	public int getMaxElet() {
		return maxElet;
	}

	public void setMaxElet(int maxElet) {
		this.maxElet = maxElet;
	}

	public int getMaxMana() {
		return maxMana;
	}

	public void setMaxMana(int maxMana) {
		this.maxMana = maxMana;
	}

	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public int getSzint() {
		return szint;
	}

	public void setSzint(int szint) {
		this.szint = szint;
	}

	public long getArany() {
		return arany;
	}

	public void setArany(long arany) {
		this.arany = arany;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nev == null) ? 0 : nev.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Karakter other = (Karakter) obj;
		if (nev == null) {
			if (other.nev != null)
				return false;
		} else if (!nev.equals(other.nev))
			return false;
		return true;
	}
	
	

}
