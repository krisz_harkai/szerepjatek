package szerepjatek.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Kepesseg {	
	@Id
	private String nev;
	private int sebzes;
	private int gyogyitas;
	private int koltseg;

	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public int getSebzes() {
		return sebzes;
	}

	public void setSebzes(int sebzes) {
		this.sebzes = sebzes;
	}

	public int getGyogyitas() {
		return gyogyitas;
	}

	public void setGyogyitas(int gyogyitas) {
		this.gyogyitas = gyogyitas;
	}

	public int getKoltseg() {
		return koltseg;
	}

	public void setKoltseg(int koltseg) {
		this.koltseg = koltseg;
	}
}
