package szerepjatek.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Osztaly {
	@Id
	private String nev;
	
	@OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
	private List<Kepesseg> kepessegek;
	
	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public List<Kepesseg> getKepessegek() {
		return kepessegek;
	}

	public void setKepessegek(List<Kepesseg> kepessegek) {
		this.kepessegek = kepessegek;
	}
}
