package szerepjatek.entities;

public enum Ritkasag {
	COMMON ("Átlagos", "gray"),
	UNCOMMON ("Különös", "white"),
	RARE ("Ritka", "blue"),
	EPIC ("Epikus", "purple"),
	LEGENDARY ("Legendás", "orange");
	
	private final String nev;
	private final String szinkod;
	
	Ritkasag(String nev, String szinkod){
		this.nev = nev;
		this.szinkod = szinkod;
	}
	
	public String getNev() {return nev;}
	public String getSzinkod() {return szinkod;}
}
