package szerepjatek.entities;

public enum Tipus {
      FEJES, NYAKLANC, VALLAS, PALAST, FELSO, CSUKLO, TABARD,
      KESZTYU, OV, NADRAG, CIPO, GYURU1, GYURU2, TRINKET1,
      MAINHAND, OFFHAND, RANGED
}
