package szerepjatek.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

@Entity
public class Uzlet {
	@Id
	private String nev;
	
	@ManyToMany(fetch = FetchType.EAGER)
	private List<Felszereles> aruk;

	public String getNev() {
		return nev;
	}

	public void setNev(String nev) {
		this.nev = nev;
	}

	public List<Felszereles> getAruk() {
		return aruk;
	}

	public void setAruk(List<Felszereles> aruk) {
		this.aruk = aruk;
	}
}
