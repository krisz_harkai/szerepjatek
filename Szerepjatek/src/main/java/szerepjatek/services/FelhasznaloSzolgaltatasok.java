package szerepjatek.services;

import java.util.List;

import szerepjatek.entities.Felhasznalo;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.entities.Osztaly;
import szerepjatek.services.exceptions.FelhSzolgException;
import szerepjatek.services.exceptions.InvalidCharException;


/**
 * A felhasználó és annak karaktereinek kezelésére
 * @author Gyakornok
 *
 */
public interface FelhasznaloSzolgaltatasok {
	/**
	 * A felhasználó regisztrálása
	 * @param nev
	 * @param felhNev
	 * @param jelszo
	 * @param email
	 * @return
	 * @throws FelhSzolgException ha már volt korábban ilyen felhasználónévvel valaki
	 */
	Felhasznalo regisztralas(String nev, String felhNev, String jelszo, String email) throws FelhSzolgException;
	
	/**
	 * Felhasználó beléptetése felhasználónév és jelszó alapján
	 * @param felhNev
	 * @param jelszo
	 * @return
	 * @throws FelhSzolgException ha már be volt lépve
	 */
	Felhasznalo belepes(String felhNev, String jelszo) throws FelhSzolgException;
	
	/**
	 * Felhasználó kiléptetése
	 * @param felhasznalo
	 * @return
	 * @throws FelhSzolgException ha még be sem volt lépve
	 */
	Felhasznalo kilepes(Felhasznalo felhasznalo) throws FelhSzolgException;
	
	/**
	 * Egy karakter létrehozása
	 * @param felhasznalo akié a karakter
	 * @param nev a karakter neve
	 * @param nem
	 * @param faj
	 * @param osztaly
	 * @param eletkor
	 * @return
	 * @throws InvalidCharException ha már létrehoztak ilyen névvel karaktert
	 */
	JatekosKarakter karakterKeszites(Felhasznalo felhasznalo, String nev,
			String nem, String faj, Osztaly osztaly, int eletkor) throws InvalidCharException;
	
	/**
	 * Egy karakter törlése
	 * @param felhasznalo
	 * @param karakter
	 * @throws InvalidCharException ha nem ehhez a felhasználóhoz tartoik
	 */
	void karakterTorles(Felhasznalo felhasznalo, JatekosKarakter karakter) throws InvalidCharException;;
	
	/**
	 * Egy karakter kiválasztása a meglévők közül
	 * @param felhasznalo
	 * @param karakter
	 * @return
	 * @throws InvalidCharException ha nem saját meglévő karaktert akarunk kiválasztani
	 */
	JatekosKarakter karakterValasztas(Felhasznalo felhasznalo, JatekosKarakter karakter) throws InvalidCharException;
	
	/**
	 * Karakterböngészde, az összes karakter visszaadásához
	 * @return
	 */
	List<JatekosKarakter> karakterBongeszes();
	
	//csak teszteléshez kell
	List<Felhasznalo> felhasznaloBongeszes();
	JatekosKarakter karakterKeres(String id);
	List<Felhasznalo> aktivok();

}
