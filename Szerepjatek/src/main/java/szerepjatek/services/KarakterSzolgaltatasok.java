package szerepjatek.services;

import szerepjatek.entities.Felhasznalo;
import szerepjatek.entities.Felszereles;
import szerepjatek.entities.GepiKarakter;
import szerepjatek.entities.Jatek;
import szerepjatek.entities.JatekSztori;
import szerepjatek.entities.JatekVegek;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.services.exceptions.InvalidCharException;;

/**
 * A karakterhez köthető dolgok
 * @author Gyakornok
 *
 */
public interface KarakterSzolgaltatasok {
	/**
	 * Egy karakter Hordva listájához való újabb felszerelés hozzáadása vagy lecserélése
	 * @param felhasznalo
	 * @param karakter
	 * @param felszereles
	 * @return
	 * @throws InvalidCharException
	 */
	JatekosKarakter karakterOltoztetes(Felhasznalo felhasznalo, JatekosKarakter karakter, Felszereles felszereles) throws InvalidCharException;
	
	/**
	 * Egy karakterenen meglévő felszerelés a Horva listából Zsak listába való áthelyezése
	 * Ez által levétele
	 * @param felhasznalo
	 * @param karakter
	 * @param felszereles
	 * @return
	 * @throws InvalidCharException
	 */
	JatekosKarakter karakterVetkeztetes(Felhasznalo felhasznalo, JatekosKarakter karakter, Felszereles felszereles) throws InvalidCharException;
	
	/**
	 * Minden sztori végén a megfelelő jutalom/büntetés kiosztása
	 * @param nyert
	 * @param karakter
	 * @param ellenfel
	 * @return
	 */
	JatekosKarakter korVegiJutalom(boolean nyert, JatekosKarakter karakter, GepiKarakter ellenfel);
	
	/**
	 * Egy felszerelés zsákba helyezése
	 * @param karakter
	 * @param felszereles
	 * @return
	 * @throws InvalidCharException
	 */
	JatekosKarakter karakterZsakbaPakolas(JatekosKarakter karakter, Felszereles felszereles) throws InvalidCharException;
	
	/**
	 * Egy felszerelés zsákból való törlése
	 * @param karakter
	 * @param felszereles
	 * @return
	 * @throws InvalidCharException
	 */
	JatekosKarakter karakterZsakbolTorles(JatekosKarakter karakter, Felszereles felszereles) throws InvalidCharException;
	
	/**
	 * Egy játék indítása
	 * @param jatekos
	 * @param jatekosKarakter
	 * @return
	 */
	Jatek jatekKezdese(Felhasznalo felhasznalo, JatekosKarakter jatekosKarakter);
	
	/**
	 * Játékon belül a sztorik listáján a következőre való lépés
	 * @param jatek
	 * @return
	 */
	JatekSztori sztoriLeptetes(Jatek jatek, JatekVegek sztoriveg, int hanyadik);
	
	/**
	 * A karakter valamint az ellenfél egy körét kezeli
	 * @param valasz a bemenet, ami alapján kiválasztjuk, mi történjen
	 * @param karakter aki véghez viszi a cselekményt
	 * @param ellenfel aki elszenvedi, illetve reagál a cselekményre
	 * @return a sztori kimenetele, JatekVegek egy eleme (átmeneti, győzelem, vereség)
	 */
	JatekVegek karakterLepes(Jatek jatek, String valasz, JatekosKarakter karakter, GepiKarakter ellenfel);
	
}
