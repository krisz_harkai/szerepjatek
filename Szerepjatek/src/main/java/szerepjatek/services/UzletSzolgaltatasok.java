package szerepjatek.services;

import szerepjatek.entities.Felhasznalo;
import szerepjatek.entities.Felszereles;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.entities.Uzlet;
import szerepjatek.services.exceptions.ArException;
import szerepjatek.services.exceptions.ShoppingException;

/**
 * A játékon belüli üzletekhez köthető interakciók
 * @author Gyakornok
 *
 */
public interface UzletSzolgaltatasok {
	
	/**
	 * Egy tárgy megvásárlása egy karakterrel
	 * @param felszereles
	 * @param karakter
	 * @return
	 * @throws ShoppingException ha nincs elég aranya
	 */
	JatekosKarakter vasarlas(Felszereles felszereles, JatekosKarakter karakter) throws ShoppingException;
	
	/**
	 * Egy felszerelés eladása
	 * @param felszereles
	 * @param karakter
	 * @return
	 * @throws ShoppingException ha nincs is a karakternél ez a felszerelés
	 */
	Felszereles eladas(Felszereles felszereles, JatekosKarakter karakter) throws ShoppingException;
		
	//admin
	/**
	 * Egy üzletben egy bizonyos felszerelés árának megváltoztatása
	 * @param uzlet
	 * @param felhasznalo
	 * @param felszereles
	 * @param ujAr
	 * @return
	 * @throws ArException amenyibben túl kicsi vagy túl nagy árat adunk meg
	 */
	boolean uzletUpdate(Uzlet uzlet, Felhasznalo felhasznalo, Felszereles felszereles, int ujAr) throws ArException;
	
	/**
	 * Egy teljes üzlet választékának leárazása
	 * @param uzlet
	 * @param felhasznalo
	 * @param szazalek
	 * @return
	 * @throws ArException ha nem 0-100 között van a százalék
	 */
	boolean uzletAkcio(Uzlet uzlet, Felhasznalo felhasznalo, int szazalek) throws ArException;
	
	Uzlet findUzlet(String nev);
}
