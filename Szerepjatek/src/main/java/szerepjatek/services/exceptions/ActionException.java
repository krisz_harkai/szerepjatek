package szerepjatek.services.exceptions;

public class ActionException extends Exception {

	public ActionException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ActionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ActionException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ActionException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ActionException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
