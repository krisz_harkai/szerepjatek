package szerepjatek.services.exceptions;

public class FelhSzolgException extends Exception {

	public FelhSzolgException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FelhSzolgException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public FelhSzolgException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public FelhSzolgException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public FelhSzolgException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
