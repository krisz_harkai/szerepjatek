package szerepjatek.services.exceptions;

public class InvalidCharException extends Exception {

	public InvalidCharException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public InvalidCharException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public InvalidCharException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidCharException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidCharException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}
