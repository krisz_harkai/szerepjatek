package szerepjatek.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import szerepjatek.dao.FelhasznaloSzolgDao;
import szerepjatek.entities.Felhasznalo;
import szerepjatek.entities.Jatek;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.entities.Jogkor;
import szerepjatek.entities.Osztaly;
import szerepjatek.services.FelhasznaloSzolgaltatasok;
import szerepjatek.services.exceptions.FelhSzolgException;
import szerepjatek.services.exceptions.InvalidCharException;

public class FelhasznaloSzolgaltatasokImpl implements FelhasznaloSzolgaltatasok {
	private final FelhasznaloSzolgDao fszdao;
	private final List<Felhasznalo> aktivFelh;

	public FelhasznaloSzolgaltatasokImpl(FelhasznaloSzolgDao felhSzDao) {
		super();
		this.fszdao = felhSzDao;
		this.aktivFelh = new ArrayList<>();
	}

	@Override
	@Transactional
	public Felhasznalo regisztralas(String nev, String felhNev, String jelszo, String email) throws FelhSzolgException {
		Felhasznalo felh = new Felhasznalo();
		felh.setNev(nev);
		felh.setFelhNev(felhNev);
		felh.setJelszo(jelszo);
		felh.setEmail(email);
		felh.setAktivKarakter(null);
		felh.setJogkor(Jogkor.JATEKOS);
		felh.setKarakterek(new ArrayList<>());
		felh.setJatekok(new ArrayList<>());

		felh = fszdao.create(felh);
		//System.out.println(felh.getKarakterek());
		if (felh == null)
			throw new FelhSzolgException("Ilyen felhasználónévvel már regisztráltak.");
		return felh;
	}

	@Override
	@Transactional
	public Felhasznalo belepes(String felhNev, String jelszo) throws FelhSzolgException {
		Felhasznalo felh = fszdao.findByNameAndPass(felhNev, jelszo);
		if (felh == null)
			throw new FelhSzolgException("Hibás felhasználónév vagy jelszó.");
		if (aktivFelh.contains(felh))
			throw new FelhSzolgException("Ezzel a felhasználónévvel már be vannak jelentkezve.");
		aktivFelh.add(felh);
		return felh;
	}

	@Override
	@Transactional
	public Felhasznalo kilepes(Felhasznalo felhasznalo) throws FelhSzolgException {
		if (felhasznalo == null || !aktivFelh.contains(felhasznalo))
			throw new FelhSzolgException("Ilyen felhasználó nincs belépve.");
		aktivFelh.remove(felhasznalo);
		return felhasznalo;
	}

	@Override
	@Transactional
	public JatekosKarakter karakterKeszites(Felhasznalo felhasznalo, String nev, String nem, String faj,
			Osztaly osztaly, int eletkor) throws InvalidCharException {

		JatekosKarakter karakter = new JatekosKarakter();
		karakter.setFelhasznalo(felhasznalo);
		karakter.setNev(nev);
		karakter.setNem(nem);
		karakter.setFaj(faj);
		karakter.setOsztaly(osztaly);
		karakter.setEletkor(eletkor);
		karakter.setArany(100);
		karakter.setElet(1000);
		karakter.setMana(1000);
		karakter.setMaxElet(1000);
		karakter.setMaxMana(1000);
		karakter.setTamadas(20);
		karakter.setVedekezes(20);
		karakter.setSzint(1);
		karakter.setTp(0);
		karakter.setKovszintTp(1000);
		karakter.setHordva(new ArrayList<>());
		karakter.setZsak(new ArrayList<>());
		
		if ((fszdao.read(JatekosKarakter.class, nev)) != null)
			throw new InvalidCharException("Ilyen névvel már létrehoztak karaktert.");
		if (felhasznalo.getAktivKarakter() == null) {
			felhasznalo.setAktivKarakter(karakter);
		}
		felhasznalo.getKarakterek().add(karakter);
		fszdao.update(felhasznalo);
		return karakter;
	}

	@Override
	@Transactional
	public void karakterTorles(Felhasznalo felhasznalo, JatekosKarakter karakter) throws InvalidCharException {
		if (!karakter.getFelhasznalo().equals(felhasznalo))
			throw new InvalidCharException("Ez a karakter nem ehhez a felhasználóhoz tartozik.");
		
		felhasznalo.getKarakterek().remove(karakter);	
		
		if (felhasznalo.getAktivKarakter().equals(karakter)) {
			if(felhasznalo.getKarakterek().size()==1){
				felhasznalo.setAktivKarakter(null);
			}
			else{
				felhasznalo.setAktivKarakter(felhasznalo.getKarakterek().get(0));
			}
		}
	
		fszdao.update(felhasznalo);

	}

	@Override
	@Transactional
	public JatekosKarakter karakterValasztas(Felhasznalo felhasznalo, JatekosKarakter karakter)
			throws InvalidCharException {
		if (!karakter.getFelhasznalo().equals(felhasznalo))
			throw new InvalidCharException("Ez a karakter nem ehhez a felhasználóhoz tartozik.");
		
		felhasznalo.setAktivKarakter(karakter);
		fszdao.update(felhasznalo);
		return karakter;
	}

	@Override
	public List<JatekosKarakter> karakterBongeszes() {
		return fszdao.findAll(JatekosKarakter.class);

	}
	
	
	
	//teszteléshez
	@Override
	public JatekosKarakter karakterKeres(String id) {
		return fszdao.read(JatekosKarakter.class, id);
	}
	@Override
	public List<Felhasznalo> felhasznaloBongeszes() {
		return fszdao.findAll(Felhasznalo.class);
	}
	@Override
	public List<Felhasznalo> aktivok(){
		return this.aktivFelh;
	}
	
	

}
