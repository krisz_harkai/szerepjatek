package szerepjatek.services.impl;

import static java.lang.Math.toIntExact;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.transaction.annotation.Transactional;

import szerepjatek.dao.KarakterSzolgDao;
import szerepjatek.entities.Felhasznalo;
import szerepjatek.entities.Felszereles;
import szerepjatek.entities.GepiKarakter;
import szerepjatek.entities.Jatek;
import szerepjatek.entities.JatekSztori;
import szerepjatek.entities.JatekVegek;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.services.KarakterSzolgaltatasok;
import szerepjatek.services.exceptions.InvalidCharException;

public class KarakterSzolgaltatasokImpl implements KarakterSzolgaltatasok {
	private final KarakterSzolgDao kszdao;
	
	public KarakterSzolgaltatasokImpl(KarakterSzolgDao kszdao) {
		super();
		this.kszdao = kszdao;
	}

	@Override
	@Transactional
	public JatekosKarakter karakterOltoztetes(Felhasznalo felhasznalo, JatekosKarakter karakter, Felszereles felszereles)
			throws InvalidCharException{
		if(!felhasznalo.getAktivKarakter().equals(karakter))
			throw new InvalidCharException("Rossz karakter van kiválasztva.");
		if(!karakter.getZsak().contains(felszereles))
			throw new InvalidCharException("Ez a felszerelés nincs a karakter zsákjában.");

		return kszdao.addHordva(karakter, felszereles);
	}
	
	@Override
	@Transactional
	public JatekosKarakter karakterVetkeztetes(Felhasznalo felhasznalo, JatekosKarakter karakter,
			Felszereles felszereles) throws InvalidCharException {
		if(!felhasznalo.getAktivKarakter().equals(karakter))
			throw new InvalidCharException("Rossz karakter van kiválasztva.");
		if(!karakter.getHordva().contains(felszereles))
			throw new InvalidCharException("Ezt a felszerelést nem hordja a karakter.");
		
		return kszdao.removeHordva(karakter, felszereles);
	}
	
	@Override
	@Transactional
	public JatekosKarakter karakterZsakbaPakolas(JatekosKarakter karakter, Felszereles felszereles) throws InvalidCharException{
		if(!karakter.getZsak().contains(felszereles))
			throw new InvalidCharException("A törlésre szánt felszerelés nincs a karakter zsákjában.");
		
		karakter.getZsak().add(felszereles);
		return kszdao.update(karakter);
	}
	

	@Override
	@Transactional
	public JatekosKarakter karakterZsakbolTorles(JatekosKarakter karakter, Felszereles felszereles) throws InvalidCharException{
		if(!karakter.getZsak().contains(felszereles))
			throw new InvalidCharException("A törlésre szánt felszerelés nincs a karakter zsákjában.");
		
		karakter.getZsak().remove(felszereles);
		return kszdao.update(karakter);
	}

	@Override
	@Transactional
	public JatekosKarakter korVegiJutalom(boolean nyert, JatekosKarakter karakter, GepiKarakter ellenfel) {
		if(nyert){
			karakter.setArany(karakter.getArany()+ellenfel.getArany());
			long tp = karakter.getTp()+ellenfel.getSzint()*120;
			karakter.setTp(tp % karakter.getKovszintTp());
			karakter.setSzint(toIntExact(tp/karakter.getKovszintTp()));
			kszdao.update(karakter);
		}
		else{
			karakter.setArany(karakter.getArany()/2);
			kszdao.update(karakter);
		}
		return karakter;
	}

	@Override
	@Transactional
	public Jatek jatekKezdese(Felhasznalo felhasznalo, JatekosKarakter jatekosKarakter) {
		List<JatekSztori> sztorik = new ArrayList<>();
		
		int i = 0;
		while(i<5){
			JatekSztori jsz = kszdao.chooseRandomStory();
			if(!sztorik.contains(jsz)){
				sztorik.add(kszdao.chooseRandomStory());
				i++;
			}
		}
		
		Jatek jatek = new Jatek();
		jatek.setFelhasznalo(felhasznalo);
		jatek.setKarakter(jatekosKarakter);
		jatek.setJatekSztorik(sztorik);
		jatek.setVege(JatekVegek.ATMENETI);
		
		felhasznalo.getJatekok().add(jatek);
		kszdao.update(felhasznalo);
		return kszdao.update(jatek);
	}

	@Override
	@Transactional
	public JatekSztori sztoriLeptetes(Jatek jatek, JatekVegek sztoriveg, int hanyadik) {
		if(sztoriveg==JatekVegek.GYOZELEM){
			if(hanyadik==5){
				jatek.setVege(JatekVegek.GYOZELEM);
				kszdao.update(jatek);
				return null;
			}
			return jatek.getJatekSztorik().get(hanyadik);	
		}
		
		jatek.setVege(JatekVegek.VERESEG);
		kszdao.update(jatek);
		return null;
	}

	
	/*___________________________________________________________________________________________*/
	/*-------------------------------------------------------------------------------------------*/
	/*--------------------Erős TODO kell ide a napokban/hetekben---------------------------------*/
	/*---------------------Vagy napokba/hetekbe?-------------------------------------------------*/
	/*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~*/
	@Override
	@Transactional
	public JatekVegek karakterLepes(Jatek jatek, String valasz, JatekosKarakter karakter, GepiKarakter ellenfel) {
		
		//String[] valaszok = {"1","2","3","4","5"};
		//String[] valaszok2 = {"tamadas","vedekezes","speci","nagyonspeci","nemtudom"};
		//támadás, védekezés, képesség1, képesség2, speciális, hezitáló
		
		switch(valasz.toLowerCase()){
			case "1":
				//ellenfelElet = ellenfelElet-karakter.getTamadas();
				break;
			case "2":;
				break;
			case "3":;
				break;
			case "4":;
				break;
			case "5":;
				break;
			default:;
				break;
		}
		
		Random rand = new Random();
		int ellenfelValasz = rand.nextInt(3)+1;
		switch(ellenfelValasz){
			case 1: 
				//jatekosElet = jatekosElet-ellenfel.getTamadas();
				break;
			case 2: ;
				break;
			case 3: ;
				break;
			default:;
				break;
		}
		
		if(karakter.getElet()<=0) return JatekVegek.VERESEG;
		if(ellenfel.getElet()<=0) return JatekVegek.GYOZELEM;
		return JatekVegek.ATMENETI;
	}

}
