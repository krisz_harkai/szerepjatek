package szerepjatek.services.impl;

import org.springframework.transaction.annotation.Transactional;

import szerepjatek.dao.UzletSzolgDao;
import szerepjatek.entities.Felhasznalo;
import szerepjatek.entities.Felszereles;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.entities.Jogkor;
import szerepjatek.entities.Uzlet;
import szerepjatek.services.UzletSzolgaltatasok;
import szerepjatek.services.exceptions.ArException;
import szerepjatek.services.exceptions.ShoppingException;

public class UzletSzolgaltatasokImpl implements UzletSzolgaltatasok {
	private final UzletSzolgDao uszdao;
	
	public UzletSzolgaltatasokImpl(UzletSzolgDao uszdao) {
		super();
		this.uszdao = uszdao;
	}

	@Override
	@Transactional
	public JatekosKarakter vasarlas(Felszereles felszereles, JatekosKarakter karakter) throws ShoppingException {
		if(karakter.getArany()<felszereles.getAr())
			throw new ShoppingException("Nincs elég aranyad a tárgy megvásárlásához.");
		karakter.setArany(karakter.getArany()-felszereles.getAr());
		karakter.getZsak().add(felszereles);		
		return uszdao.update(karakter);
	}

	@Override
	@Transactional
	public Felszereles eladas(Felszereles felszereles, JatekosKarakter karakter) throws ShoppingException {
		if(!karakter.getZsak().contains(felszereles))
			throw new ShoppingException("Az eladásra szánt tárgy nincs a birtokodban.");
		karakter.setArany(karakter.getArany()+felszereles.getAr()/2);
		karakter.getZsak().remove(felszereles);
		
		uszdao.update(karakter);
		return felszereles;
	}

	@Override
	@Transactional
	public boolean uzletUpdate(Uzlet uzlet, Felhasznalo felhasznalo, Felszereles felszereles, int ujAr) throws ArException{
		if(ujAr<0 || ujAr>999999) throw new ArException("Az árnak 0 és 999999 között kell lennie.");
		if(felhasznalo.getJogkor()==Jogkor.ADMIN){
			if(!uzlet.getAruk().contains(felszereles))
				throw new ArException("Ez az üzlet nem tartalmazza ezt a felszerelést");
			felszereles.setAr(ujAr);
			uszdao.update(uzlet);
			return true;
		}
		return false;
	}

	@Override
	@Transactional
	public boolean uzletAkcio(Uzlet uzlet, Felhasznalo felhasznalo, int szazalek) throws ArException{
		if(szazalek<0 || szazalek>100) throw new ArException("A leárazás százalékban 0 és 100 közé kell hogy essen.");
		if(felhasznalo.getJogkor()==Jogkor.ADMIN){
			for (Felszereles felsz : uzlet.getAruk()) {
				felsz.setAr(felsz.getAr()-felsz.getAr()*szazalek/100);
				uszdao.update(uzlet);
			}
			return true;
		}
		return false;
	}

	@Override
	public Uzlet findUzlet(String nev) {
		return uszdao.read(Uzlet.class, nev);
	}
	
	

}
