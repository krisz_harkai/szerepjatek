package szerepjatek.vaadin;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.Page;
import com.vaadin.server.Page.Styles;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Audio;
import com.vaadin.ui.Button;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.VerticalLayout;

import szerepjatek.entities.JatekosKarakter;

public class ActionBar extends GridLayout{
	
	private Button attack;
	private Button spell1;
	private Button spell2;
	private Button spell3;
	private Button spell4;
	private Button spell5;
	private Button spell6;
	private Button random;
	private Audio audio;
	Styles styles;
	
	
	
	public Button getAttack() {
		return attack;
	}

	public Button getSpell1() {
		return spell1;
	}

	public Button getSpell2() {
		return spell2;
	}

	public Button getSpell3() {
		return spell3;
	}

	public ActionBar(JatekosKarakter karakter){
		audio = new Audio("", new ThemeResource("sounds/ladyn.mp3"));
		audio.setShowControls(false);
		
		styles = Page.getCurrent().getStyles();	
		setSizeFull();
		setRows(1);
		setColumns(9);
		
		attack=buttonolas("attack-button","1");
		spell1=buttonolas("spell1-button","2");
		spell2=buttonolas("spell2-button","3");
		spell3=buttonolas("spell3-button","4");
		spell4=buttonolas("spell4-button","5");
		spell5=buttonolas("spell5-button","6");
		spell6=buttonolas("spell6-button","7");
		random=buttonolas("random-button","8");
		
		attack.setClickShortcut(KeyCode.NUM1);
		spell1.setClickShortcut(KeyCode.NUM2);
		spell2.setClickShortcut(KeyCode.NUM3);
		spell3.setClickShortcut(KeyCode.NUM4);
		
		
		attack.setDescription("<p>Támadás</p>"
				+ "<p>Csak sima kis ütögetés, semmi komoly.</p>");
		spell3.setDescription("<img src=\"gyuri.jpg\"/>");
		spell3.addClickListener(e->{
		     audio.play();
		});
		
		
		addComponent(attack, 0, 0);
		addComponent(spell1, 1, 0);
		addComponent(spell2, 2, 0);
		addComponent(spell3, 3, 0);
		addComponent(spell4, 4, 0);
		addComponent(spell5, 5, 0);
		addComponent(spell6, 6, 0);
		addComponent(random, 7, 0);
		addComponent(audio, 8,0);
		
		setComponentAlignment(attack, Alignment.MIDDLE_CENTER);
		setComponentAlignment(spell1, Alignment.MIDDLE_CENTER);
		setComponentAlignment(spell2, Alignment.MIDDLE_CENTER);
		setComponentAlignment(spell3, Alignment.MIDDLE_CENTER);
		setComponentAlignment(spell4, Alignment.MIDDLE_CENTER);
		setComponentAlignment(spell5, Alignment.MIDDLE_CENTER);
		setComponentAlignment(spell6, Alignment.MIDDLE_CENTER);
		setComponentAlignment(random, Alignment.MIDDLE_CENTER);
		
		spell4.setEnabled(false);
		spell5.setEnabled(false);
		spell6.setEnabled(false);
		random.setEnabled(false);
		
		setStyleName("action-bar");
	}
	
	private Button buttonolas(String stylename, String kepnev){
		Button button = new Button();
		button.setStyleName(stylename);
		button.setWidth("65px");
		button.setHeight("65px");
		
		
        styles.add(".v-app ."+stylename+" { "
        		+ "background: url(VAADIN/themes/mytheme/pictures/Ikonok/"+kepnev+".jpg);"
        		+ "background-size: 100% 100%;"
        		+ "border: 2px solid black;"
        		+ "}");
        
        return button;
	}
	
}
