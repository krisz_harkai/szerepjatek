package szerepjatek.vaadin;

import com.vaadin.server.Page;
import com.vaadin.server.Page.Styles;
import com.vaadin.ui.Button;
import com.vaadin.ui.Layout;

import szerepjatek.entities.Felszereles;

public class FelszerelesButton extends Button{
	private Felszereles felsz;
		
	public FelszerelesButton(Felszereles f){
		this.felsz=f;
		
		addStyleName("felszereles-gomb-"+felsz.getNev());
		Styles styles = Page.getCurrent().getStyles();
        
        styles.add(".v-app .felszereles-gomb-" + felsz.getNev() + " { "
        		+ "box-shadow: inset 0px 0px 16px 2px "+felsz.getRitkasag().getSzinkod()+";"
        		+ "border-radius: 2px;"
        		+ "border: 1px solid " +felsz.getRitkasag().getSzinkod()+ "; "
        		+ "background: url(VAADIN/themes/mytheme/pictures/Itemek/"+felsz.getTipus()+".png);"
        		+ "background-size: 100% 100%;"
        		+ "}");
		
		setDescription( 
				"<p style=\"color: "+felsz.getRitkasag().getSzinkod()+";\">"+felsz.getRitkasag().getNev()+" "+felsz.getNev()+"</p>"+
				"<ul>"+
				"  <li>Ára: " + felsz.getAr() + " aranytallér</li>"+
				"  <li>Élet: " + felsz.getElet()+"</li>"+
				"  <li>Mana: " + felsz.getMana() + "</li>"+
				"  <li>Támadás: " + felsz.getTamadas() + "</li>"+
				"  <li>Védekezés: " + felsz.getVedekezes() + "</li>"+
				"</ul>"+
				"<p>"+felsz.getLeiras()+"<p>");
		
		setHeight("65px");
		setWidth("70px");
		//setCaption(felsz.getNev());
		addStyleName("felszereles-gomb");
	}
	
	public Felszereles getFelszereles(){
		return this.felsz;
	}
}
