package szerepjatek.vaadin;

import com.vaadin.server.Page;
import com.vaadin.server.Page.Styles;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;

import szerepjatek.entities.Felszereles;

public class FelszerelesLayout extends HorizontalLayout{
	private Felszereles felsz;
	private Label kep;
	private Label nev;
	private Label osszeg;
	
	public FelszerelesLayout(Felszereles f){
		this.felsz=f;
		this.kep = new Label();
		kep.setStyleName("felszereles-kep-"+felsz.getNev());
		this.nev = new Label(felsz.getNev());
		this.osszeg = new Label(Integer.toString(felsz.getAr())+" aranytallér");
		
		addStyleName("felszereles-layout-"+felsz.getNev());
		Styles styles = Page.getCurrent().getStyles();
        styles.add(".v-app .felszereles-layout-" + felsz.getNev() + " { " 
        		+ "margin:3px;"
        		+ "box-shadow: inset 0px 0px 16px 2px "+felsz.getRitkasag().getSzinkod()+";"
        		+ "border-radius: 2px;"
        		+ "border: 1px solid " +felsz.getRitkasag().getSzinkod()+ "; "
        		+ "background: black;"
        		+ "}");
        
        styles.add(".v-app .felszereles-kep-" + felsz.getNev() + " { "
        		+ "box-shadow: inset 0px 0px 16px 2px "+felsz.getRitkasag().getSzinkod()+";"
        		+ "border-radius: 2px;"
        		+ "border: 1px solid " +felsz.getRitkasag().getSzinkod()+ "; "
        		+ "background: url(VAADIN/themes/mytheme/pictures/Itemek/"+felsz.getTipus()+".png);"
        		+ "background-size: 100% 100%;"
        		+ "}");
        
        setDescription( 
				"<p style=\"color: "+felsz.getRitkasag().getSzinkod()+";\">"+felsz.getRitkasag().getNev()+" "+felsz.getNev()+"</p>"+
				"<ul>"+
				"  <li>Ára: " + felsz.getAr() + " aranytallér</li>"+
				"  <li>Élet: " + felsz.getElet()+"</li>"+
				"  <li>Mana: " + felsz.getMana() + "</li>"+
				"  <li>Támadás: " + felsz.getTamadas() + "</li>"+
				"  <li>Védekezés: " + felsz.getVedekezes() + "</li>"+
				"</ul>"+
				"<p>"+felsz.getLeiras()+"<p>");
		
		setWidth(100, Unit.PERCENTAGE);
		setHeight(60, Unit.PIXELS);
		//setCaption(felsz.getNev());
		//addStyleName("felszereles-button");
		addComponents(kep,nev,osszeg);
		setComponentAlignment(nev, Alignment.MIDDLE_LEFT);
		setComponentAlignment(osszeg, Alignment.MIDDLE_RIGHT);
		
		kep.setWidth(56, Unit.PIXELS);
		kep.setHeight(56, Unit.PIXELS);
		
	}
}
