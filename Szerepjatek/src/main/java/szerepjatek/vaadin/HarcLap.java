package szerepjatek.vaadin;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import szerepjatek.entities.GepiKarakter;
import szerepjatek.entities.JatekosKarakter;

public class HarcLap extends Window {	
	private HorizontalLayout windowContent;
	private VerticalLayout karakter;
	private Label karakterKep;
	private ProgressBar karakterElet;
	private ProgressBar karakterMana;
	private TextArea combatlog;
	private VerticalLayout ellenfel;
	private Label ellenfelKep;
	private ProgressBar ellenfelElet;
	private ProgressBar ellenfelMana;
	
	public HarcLap(JatekosKarakter kar, GepiKarakter ell){
		
		
		windowContent = new HorizontalLayout();
		karakter = new VerticalLayout();
		karakterKep = new Label();
		karakterElet = new ProgressBar((float)kar.getElet()/kar.getMaxElet());
		karakterMana = new ProgressBar((float)kar.getMana()/kar.getMaxMana());
		combatlog = new TextArea();
		ellenfel = new VerticalLayout();
		ellenfelKep = new Label();
		ellenfelElet = new ProgressBar((float)ell.getElet()/ell.getMaxElet());
		ellenfelMana = new ProgressBar((float)ell.getMana()/ell.getMaxMana());
		
		windowContent.addComponents(karakter, combatlog, ellenfel);
		windowContent.setSizeFull();
		windowContent.setMargin(true);
		windowContent.setSpacing(true);
		windowContent.setExpandRatio(karakter, 0.4f);
		windowContent.setExpandRatio(combatlog, 0.4f);
		windowContent.setExpandRatio(ellenfel, 0.4f);
		windowContent.setComponentAlignment(combatlog, Alignment.BOTTOM_CENTER);
		
		karakter.setSizeFull();
		karakter.addComponents(karakterKep, karakterElet, karakterMana);
		karakter.setExpandRatio(karakterKep, 0.8f);
		karakter.setExpandRatio(karakterElet, 0.05f);
		karakter.setExpandRatio(karakterMana, 0.05f);
		karakterKep.setSizeFull();
		karakterKep.addStyleName("karakter-harc-kep");
		karakterKep.setStyleName("profil-kep");
		karakterElet.setSizeFull();
		karakterElet.setStyleName("elet-csik");
		karakterMana.setSizeFull();
		karakterMana.setStyleName("mana-csik");
		
		combatlog.setWidth(100, Unit.PERCENTAGE);
		combatlog.setHeight(50, Unit.PERCENTAGE);
		
		ellenfel.setSizeFull();
		ellenfel.addComponents(ellenfelKep, ellenfelElet, ellenfelMana);
		ellenfel.setExpandRatio(ellenfelKep, 0.8f);
		ellenfel.setExpandRatio(ellenfelElet, 0.05f);
		ellenfel.setExpandRatio(ellenfelMana, 0.05f);
		ellenfelKep.setSizeFull();
		ellenfelKep.setStyleName("ellenfel-harc-kep");
		ellenfelElet.setSizeFull();
		ellenfelElet.setStyleName("elet-csik");
		ellenfelMana.setSizeFull();
		ellenfelMana.setStyleName("mana-csik");
		
		addStyleName("windowok");
		setContent(windowContent);
		setCaption("Harc a gonosz paci ellen");
		setWidth("1200px");
		setHeight("70%");
		setClosable(true);
		setDraggable(true);
		setResizable(false);
		//windo.setResponsive(true);
		center();
	}
	
	public void feltoltes(JatekosKarakter kar, GepiKarakter ell){
		ellenfelElet.setValue((float)ell.getElet()/ell.getMaxElet());
		ellenfelMana.setValue((float)ell.getMana()/ell.getMaxMana());
		combatlog.setValue("Paci elszenvedett egy komolyabb csapást. Visszatámadni sem tud.");
	}
	
}
