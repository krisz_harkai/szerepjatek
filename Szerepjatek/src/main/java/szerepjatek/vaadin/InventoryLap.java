package szerepjatek.vaadin;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import szerepjatek.entities.Felszereles;
import szerepjatek.entities.JatekosKarakter;


public class InventoryLap extends Window {	
	private final int bagVizszintes = 6;
	private final int bagFuggoleges = 6;
	
	private VerticalLayout windowContent;
	
	private GridLayout zsak;
	
	
	public GridLayout getZsak() {
		return zsak;
	}

	public InventoryLap(JatekosKarakter karakter){
		windowContent = new VerticalLayout();
		zsak = new GridLayout(bagVizszintes, bagFuggoleges);
		
		gridKeszites(zsak);
		
		feltoltes(karakter);
		
		windowContent.setSizeFull();
		windowContent.addComponent(zsak);
		
		addStyleName("windowok");
		setContent(windowContent);
		setWidth("460px");
		setHeight("470px");
		setClosable(true);
		setDraggable(true);
		setResizable(false);
		setCaption("Batyu");
		center();
	}
	
	private void gridKeszites(GridLayout zsak){
		zsak.setSizeFull();
		
		for (int i = 0; i < bagVizszintes; i++) {
			for (int j = 0; j < bagFuggoleges; j++) {
				VerticalLayout layout = new VerticalLayout(/*new Button("Fejes")*/);
				layout.setWidth(100, Unit.PERCENTAGE);
				layout.setHeight(100, Unit.PERCENTAGE);
				layout.setStyleName("item-hatter");
				
				zsak.addComponent(layout,j,i);
			}
		}	
	}
	
	public void feltoltes(JatekosKarakter karakter){
		/*
		 * Szóval mi is történik itt?
		 * Maximum 36* írjon ki, erre kell a maxMeret, ne csorduljon túl, ha sok az item
		 * ideiglenes megoldás
		 * j pedig kell a sorszám léptetéséhez
		 * Biztos van szebb meg jobb, de nekem tetszik!
		 */
		int maxMeret = bagVizszintes*bagFuggoleges;
		int j = -1;
		for (int i = 0; i < maxMeret; i++) {
			if(i%6==0) j++;
			VerticalLayout layout = (VerticalLayout)zsak.getComponent(i%6, j);
			layout.removeAllComponents();
			if(i<karakter.getZsak().size()){
				FelszerelesButton felszButton = new FelszerelesButton(karakter.getZsak().get(i));
				layout.addComponent(felszButton);
			}	
		}
	}
}
