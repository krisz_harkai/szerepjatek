package szerepjatek.vaadin;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import com.vaadin.annotations.Title;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.Page.Styles;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.OptionGroup;

import szerepjatek.entities.Felhasznalo;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.entities.Osztaly;
import szerepjatek.vaadin.presenters.KarakterPresenter;
import szerepjatek.vaadin.presenters.LoginPresenter;

@Title("Karakter kreálás")
@SpringView(name = "charcreate_screen")
@Scope(value = "prototype", proxyMode = ScopedProxyMode.INTERFACES)
public class KarakterKrealas extends VerticalLayout implements View {

	private VerticalLayout form;
	private Label cim;
	
	private HorizontalLayout baff;
	private OptionGroup faj;
	private OptionGroup osztaly;
	private OptionGroup nem;
	
	private TextField nev;
	
	private HorizontalLayout gombok;
	private Button letrehozas;
	private Button megsem;
	
	private Label hatterVideo;

	@Autowired
	private KarakterPresenter presenter;

	public KarakterKrealas() {
		form = new VerticalLayout();
		cim = new Label("Karakter készítés");
		
		baff = new HorizontalLayout();
		faj = new OptionGroup("Faj");
		osztaly = new OptionGroup("Osztály");
		nem = new OptionGroup("Nem");
		
		nev = new TextField();
		
		gombok = new HorizontalLayout();
		letrehozas = new Button("Létrehozás");
		megsem = new Button("Mégsem");
		
		hatterVideo = new Label("<video playsinline autoplay muted loop poster=\"pictures/loading.png\" id=\"fullscreen-video\">"
				+ "<source src=\"world_bg.mp4\" type=\"video/mp4\">" + "</video>", ContentMode.HTML);
	}

	@PostConstruct
	private void postInit() {
		setSizeFull();
		addStyleName("fullscreen-layout");
		addComponent(hatterVideo);
		addComponent(form);
		
		letrehozas.addStyleName("buttonee");
		megsem.addStyleName("buttonee");
		letrehozas.setWidth("100%");
		megsem.setWidth("100%");
		

		form.setWidth("500px");
		form.setMargin(true);
		form.setSpacing(true);
		form.addComponents(cim, baff, nev, gombok);
		form.addStyleName("login-form");
		
		baff.setSizeFull();
		baff.addComponents(nem, faj, osztaly);
		
		nem.setHtmlContentAllowed(true);
		nem.setSizeFull();
		nem.addStyleName("nemek");
		nem.addItems( "ferfi" , "no");
		nem.setItemCaption( "ferfi" , "<label id=\"ferfi\"></label>" );
		nem.setItemCaption( "no" , "<label id=\"no\"></label>" );
		nem.select("ferfi");
		
		//nem.setEnabled(false);
		
		faj.setHtmlContentAllowed(true);
		faj.setSizeFull();
		faj.addStyleName("fajok");
		faj.addItems( "faj1" , "faj2", "faj3", "faj4" );
		faj.setItemCaption( "faj1" , "<label id=\"faj1\"></label>" );
		faj.setItemCaption( "faj2" , "<label id=\"faj2\"></label>" );
		faj.setItemCaption( "faj3" , "<label id=\"faj3\"></label>" );
		faj.setItemCaption( "faj4" , "<label id=\"faj4\"></label>" );
		faj.select("faj1");
		
		//faj.setEnabled(false);
		
		osztaly.setHtmlContentAllowed(true);
		osztaly.setSizeFull();
		osztaly.addStyleName("osztalyok");
		osztaly.addItems( "osztaly1" , "osztaly2", "osztaly3", "osztaly4" );
		osztaly.setItemCaption( "osztaly1" , "<label id=\"osztaly1\"></label>" );
		osztaly.setItemCaption( "osztaly2" , "<label id=\"osztaly2\"></label>" );
		osztaly.setItemCaption( "osztaly3" , "<label id=\"osztaly3\"></label>" );
		osztaly.setItemCaption( "osztaly4" , "<label id=\"osztaly4\"></label>" );
		osztaly.select("osztaly1");
		
		//osztaly.setEnabled(false);
		
		nev.setSizeFull();
		
		gombok.setSizeFull();
		gombok.addComponents(megsem,letrehozas);
		
		hatterVideo.addStyleName("fullscreen-video-label");
	}

	@Override
	public void enter(ViewChangeEvent event) {	
		if(MyUI.getCurrent().getUser().getKarakterek().isEmpty()) megsem.setEnabled(false);
		megsem.addClickListener(e -> MyUI.getCurrent().getNavigator().navigateTo("main_page"));
		letrehozas.addClickListener(this::karakterKrealasEvent);
	}
	
	private void karakterKrealasEvent(ClickEvent event) {
		try {
			if (nev.getValue().trim().isEmpty()) {
				createAndShowNotification("Név megadása kötelező.", Type.WARNING_MESSAGE);
			} else {
				JatekosKarakter karakter = presenter.karakterKreal(MyUI.getCurrent().getUser(),
						nev.getValue(), "ferfi", "human", null, 30); //null ameddig nincs databáze
				if (karakter != null) {
					
					createAndShowNotification("Sikeresen létrejött a karakter.", Type.HUMANIZED_MESSAGE);
					MyUI.getCurrent().getNavigator().navigateTo("main_page");
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
			createAndShowNotification("Ilyen névvel már létrehoztak karaktert.", Type.ERROR_MESSAGE);
		}
	}
	
	public void createAndShowNotification(String caption, Notification.Type type) {
	    Notification notif = new Notification(caption, type);
	    notif.setPosition(Notification.POSITION_CENTERED_TOP);
	    notif.setDelayMsec(2000);
	    notif.show(Page.getCurrent());
	}

}
