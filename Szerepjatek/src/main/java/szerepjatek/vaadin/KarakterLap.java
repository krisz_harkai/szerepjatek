package szerepjatek.vaadin;

import java.util.HashMap;
import java.util.Iterator;

import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import szerepjatek.entities.Felszereles;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.entities.Tipus;

public class KarakterLap extends Window {	
	private HorizontalLayout windowContent;
	private GridLayout hordva;
	private Label kep;
	private TextArea adatok;
	
	private HashMap<Tipus, VerticalLayout> map = new HashMap<>();
	
	private VerticalLayout fejes = new VerticalLayout();
	private VerticalLayout nyaklanc = new VerticalLayout();
	private VerticalLayout vallas = new VerticalLayout();
	private VerticalLayout palast = new VerticalLayout();
	private VerticalLayout felso = new VerticalLayout();
	private VerticalLayout tabard = new VerticalLayout();
	private VerticalLayout csuklo = new VerticalLayout();
	
	private VerticalLayout ov = new VerticalLayout();
	private VerticalLayout kesztyu = new VerticalLayout();
	private VerticalLayout nadrag = new VerticalLayout();
	private VerticalLayout cipo = new VerticalLayout();
	private VerticalLayout gyuru1 = new VerticalLayout(); //csúnya, át kell a servicet alakítani
	private VerticalLayout gyuru2 = new VerticalLayout();
	private VerticalLayout trinket1 = new VerticalLayout();
	
	private VerticalLayout mainhand = new VerticalLayout();
	private VerticalLayout offhand = new VerticalLayout();
	private VerticalLayout ranged = new VerticalLayout();
	
	public KarakterLap(){
		windowContent = new HorizontalLayout();
		hordva = new GridLayout(7, 8);
		kep = new Label();
		adatok = new TextArea();
		
		gridKeszites(hordva);
	
		kep.setSizeFull();
		kep.setStyleName("profil-kep");		
	
		adatok.setSizeFull();
			
		windowContent.setSizeFull();
		windowContent.addComponents(hordva, adatok);
		//de ki lett ez matekozva more, 7/8*6/9
		windowContent.setExpandRatio(hordva, 0.58275f);
		windowContent.setExpandRatio(adatok, 0.41725f);
		
		setContent(windowContent);
		setWidth(900, Unit.PIXELS);
		setHeight(600, Unit.PIXELS);
		setClosable(true);
		setDraggable(true);
		setResizable(false);
		setCaption("Karakterlap");
		addStyleName("windowok");
		//windo.setResponsive(true);
		center();
	}
	
	public KarakterLap(JatekosKarakter karakter){
		windowContent = new HorizontalLayout();
		hordva = new GridLayout(7, 8);
		kep = new Label();
		adatok = new TextArea();
		
		map.put(Tipus.FEJES, fejes);
		map.put(Tipus.NYAKLANC, nyaklanc);
		map.put(Tipus.VALLAS, vallas);
		map.put(Tipus.PALAST, palast);
		map.put(Tipus.FELSO, felso);
		map.put(Tipus.TABARD, tabard);
		map.put(Tipus.CSUKLO, csuklo);
		
		map.put(Tipus.OV, ov);
		map.put(Tipus.KESZTYU, kesztyu);
		map.put(Tipus.NADRAG, nadrag);
		map.put(Tipus.CIPO, cipo);
		map.put(Tipus.GYURU1, gyuru1);
		map.put(Tipus.GYURU2, gyuru2);
		map.put(Tipus.TRINKET1, trinket1);
		
		map.put(Tipus.MAINHAND, mainhand);
		map.put(Tipus.OFFHAND, offhand);
		map.put(Tipus.RANGED, ranged);
		
		feltoltes(karakter);
		gridKeszites(hordva);
	
		kep.setSizeFull();
		kep.setStyleName("profil-kep");		
			
		windowContent.setSizeFull();
		windowContent.addComponents(hordva, adatok);
		//de ki lett ez matekozva more, 7/8*6/9
		windowContent.setExpandRatio(hordva, 0.58275f);
		windowContent.setExpandRatio(adatok, 0.41725f);
		
		setContent(windowContent);
		setWidth(900, Unit.PIXELS);
		setHeight(600, Unit.PIXELS);
		setClosable(true);
		setDraggable(true);
		setResizable(false);
		setCaption("Karakterlap");
		addStyleName("windowok");
		//windo.setResponsive(true);
		center();
	}
	
	private void gridKeszites(GridLayout hordva){
		hordva.setSizeFull();
		hordva.addComponent(kep, 1,0,5,6);
		//hordva.setSpacing(true);
		hordva.setStyleName("karakterlap-grid");
		
	    for (VerticalLayout layout : map.values()) {
	    	layout.setWidth(100, Unit.PERCENTAGE);
			layout.setHeight(100, Unit.PERCENTAGE);
			layout.setStyleName("item-hatter");
		}
		
	    hordva.addComponent(fejes,0,0);
	    //fejes.setSizeFull();
	    //fejes.setStyleName("item-hatter");
	    hordva.addComponent(nyaklanc,0,1);
	    hordva.addComponent(vallas,0,2);
	    hordva.addComponent(palast,0,3);
	    hordva.addComponent(felso,0,4);
	    hordva.addComponent(tabard,0,5);
	    hordva.addComponent(csuklo,0,6);
	    
	    hordva.addComponent(kesztyu,6,0);
	    hordva.addComponent(ov,6,1);
	    hordva.addComponent(nadrag,6,2);
	    hordva.addComponent(cipo,6,3);
	    hordva.addComponent(gyuru1,6,4);
	    hordva.addComponent(gyuru2,6,5);
	    hordva.addComponent(trinket1,6,6);
	    
	    hordva.addComponent(mainhand,2,7);
	    hordva.addComponent(offhand,3,7);
	    hordva.addComponent(ranged,4,7);
		
	}
	
	public void feltoltes(JatekosKarakter karakter){
		for (Felszereles felsz : karakter.getHordva()) {
			map.get(felsz.getTipus()).removeAllComponents();
			map.get(felsz.getTipus()).addComponent(new FelszerelesButton(felsz));
		}
		
		adatok.setReadOnly(false);
		adatok.setSizeFull();
		adatok.setValue("\t\t\t\t"+karakter.getNev()+"\n"
						//+ "Faj:\t"+karakter.getNem()+" "+karakter.getFaj()+"\n"
						//+ "Osztály:\t"+karakter.getOsztaly()+"\n"
						//+ "Szint:\t"+karakter.getSzint()+"\n\n"
						+ "\t\t\tLevel "+karakter.getSzint()+" "+karakter.getFaj()+" "+karakter.getOsztaly()+"\n\n"
						+ "\t\tArany:\t\t\t\t"+karakter.getArany()+"\n"
						+ "\t\tExp:\t\t\t\t"+karakter.getTp()+"/"+karakter.getKovszintTp()+"\n\n"
						+ "\t\tÉlet:\t\t\t\t"+karakter.getElet()+"/"+karakter.getMaxElet()+"\n"
						+ "\t\tMana:\t\t\t"+karakter.getMana()+"/"+karakter.getMaxMana()+"\n"
						+ "\t\tTámadás:\t\t"+karakter.getTamadas()+"\n"
						+ "\t\tVédekezés:\t\t"+karakter.getTamadas()+"\n");
		adatok.setReadOnly(true);	
	}
}
