package szerepjatek.vaadin;

import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.VerticalLayout;

import szerepjatek.entities.JatekosKarakter;

public class KarakterUnitFrames extends HorizontalLayout{
	private Label profil;
	private VerticalLayout adatok;
	private AbsoluteLayout elet;
	private AbsoluteLayout mana;
	private Label nevszint;
	private Label eletLabel;
	private ProgressBar eletCsik;
	private Label manaLabel;
	private ProgressBar manaCsik;
	
	public KarakterUnitFrames(JatekosKarakter karakter){
		profil = new Label();
		adatok = new VerticalLayout();
		elet = new AbsoluteLayout();
		mana = new AbsoluteLayout();
		nevszint = new Label(karakter.getNev()+" " + karakter.getSzint());
		eletLabel = new Label(karakter.getElet()+"/"+karakter.getMaxElet());
		eletCsik = new ProgressBar((float)karakter.getElet()/karakter.getMaxElet());
		manaLabel = new Label(karakter.getMana()+"/"+karakter.getMaxMana());
		manaCsik = new ProgressBar((float)karakter.getMana()/karakter.getMaxMana());
	
		profil.setSizeFull();
		profil.addStyleName("profil-kep");
		
		adatok.setSizeFull();
		adatok.addComponents(elet, mana);
		adatok.setExpandRatio(elet, 0.6f);
		adatok.setExpandRatio(mana, 0.4f);
		
		elet.setSizeFull();
		elet.addComponent(eletCsik, "left: 0px; top: 0px;");;
		elet.addComponent(nevszint, "left: 4%; top: 30%;");
		elet.addComponent(eletLabel, "right: 4%; top: 30%;");
		
		nevszint.setSizeFull();
		eletLabel.setSizeFull();
		eletLabel.setStyleName("szoveg-csik");
		eletCsik.setSizeFull();
		eletCsik.setStyleName("elet-csik");
		
		mana.setSizeFull();
		mana.addComponent(manaCsik, "left: 0px; top: 0px;");
		mana.addComponent(manaLabel, "right: 4%; top: 20%;");
		
	
		manaLabel.setSizeFull();
		manaLabel.setStyleName("szoveg-csik");
		manaCsik.setSizeFull();
		manaCsik.setStyleName("mana-csik");
		
		
		addComponents(profil, adatok);
		setExpandRatio(profil, 1);
		setExpandRatio(adatok, 2);
		setWidth("300px");
		setHeight("100px");
		setStyleName("party-karakter");
	}
	
}
