package szerepjatek.vaadin;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import com.vaadin.annotations.Title;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Notification.Type;

import szerepjatek.entities.Felhasznalo;
import szerepjatek.vaadin.presenters.LoginPresenter;

@Title("Login")
@SpringView(name = "login_screen")
@Scope(value = "prototype", proxyMode = ScopedProxyMode.INTERFACES)
public class Login extends VerticalLayout implements View {

	private Button regButton;
//	private FormLayout form;
	private Label cim;
	private TextField nev;
	private PasswordField jelszo;
	private Button belepes;
//	private Label hatterVideo;

	@Autowired
	private LoginPresenter presenter;

//	public Login() {
//		form = new FormLayout();
//		cim = new Label("Belépés");
//		nev = new TextField();
//		jelszo = new PasswordField();
//		belepes = new Button("Belépés");
//		regButton = new Button("Még nincs felhasználói fiókod? Regisztrálj egyet!");
//		hatterVideo = new Label("<video playsinline autoplay muted loop poster=\"pictures/loading.png\" id=\"fullscreen-video\">"
//				+ "<source src=\"world_bg.mp4\" type=\"video/mp4\">" + "</video>", ContentMode.HTML);
//	}

	@PostConstruct
	private void postInit() {
		
		FormLayout form = new FormLayout();
		cim = new Label("Belépés");
		nev = new TextField();
		jelszo = new PasswordField();
		belepes = new Button("Belépés");
		regButton = new Button("Még nincs felhasználói fiókod? Regisztrálj egyet!");
		Label hatterVideo = new Label("<video playsinline autoplay muted loop poster=\"pictures/loading.png\" id=\"fullscreen-video\">"
				+ "<source src=\"world_bg.mp4\" type=\"video/mp4\">" + "</video>", ContentMode.HTML);

		belepes.addStyleName("buttonee");
		belepes.setWidth("100%");
		setSizeFull();
		addStyleName("fullscreen-layout");
		addComponent(hatterVideo);
		addComponent(form);

		form.setWidth("500px");

		nev.setWidth(100, Unit.PERCENTAGE);
		nev.setInputPrompt("Felhasználónév");
		nev.getValue();

		jelszo.setWidth(100, Unit.PERCENTAGE);
		jelszo.setInputPrompt("jelszó");
		jelszo.getValue();

		regButton.addStyleName("link");

		form.addComponents(cim, nev, jelszo, belepes, regButton);
		form.addStyleName("login-form");

		hatterVideo.addStyleName("fullscreen-video-label");
	}

	@Override
	public void enter(ViewChangeEvent event) {
		nev.focus();
		
		regButton.addClickListener(e -> MyUI.getCurrent().getNavigator().navigateTo("regisztracio_screen"));

		//belepes.addClickListener(e -> MyUI.getCurrent().getNavigator().navigateTo("main_page"));
		belepes.addClickListener(this::belepesEvent);
	}

	
	private void belepesEvent(ClickEvent event) {
		try {
			if (nev.getValue().trim().isEmpty() || jelszo.getValue().trim().isEmpty()) {
				createAndShowNotification("Minden mező kitöltése kötelező.", Type.WARNING_MESSAGE);
			} else {
				Felhasznalo user = presenter.beleptet(nev.getValue(), jelszo.getValue());
				if (user != null) {
					MyUI.getCurrent().setUser(user);
					if(user.getKarakterek().isEmpty()) MyUI.getCurrent().getNavigator().navigateTo("charcreate_screen");
					else MyUI.getCurrent().getNavigator().navigateTo("main_page");
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
			createAndShowNotification("Hibás felhasználónév vagy jelszó.", Type.ERROR_MESSAGE);
		}
	}
	
	public void createAndShowNotification(String caption, Notification.Type type) {
	    Notification notif = new Notification(caption, type);
	    notif.setPosition(Position.TOP_CENTER);
	    notif.setDelayMsec(2000);
	    notif.show(Page.getCurrent());
	}
}
