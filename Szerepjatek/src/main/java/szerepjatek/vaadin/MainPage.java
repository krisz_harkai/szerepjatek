package szerepjatek.vaadin;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import com.vaadin.annotations.Title;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.server.Page;
import com.vaadin.server.ThemeResource;
import com.vaadin.server.Page.Styles;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.server.WebBrowser;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Audio;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.ProgressBar;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Notification.Type;

import szerepjatek.entities.Felhasznalo;
import szerepjatek.entities.Felszereles;
import szerepjatek.entities.GepiKarakter;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.vaadin.presenters.LoginPresenter;
import szerepjatek.vaadin.presenters.MainPagePresenter;

@Title("Main page")
@SpringView(name = "main_page")
@Scope(value = "prototype", proxyMode = ScopedProxyMode.INTERFACES)
public class MainPage extends VerticalLayout implements View {
	
	private GepiKarakter ellenfel;
	
	@Autowired
	private LoginPresenter presenterLogin;
	
	@Autowired
	private MainPagePresenter presenterMain;
	
	// felső sáv
	private CssLayout party;
	private KarakterUnitFrames aktivKarakter;

	// középső sáv, technikailag ide tartozik a jobb alul lévő menü
	private HorizontalLayout kozepso;
	private KarakterLap karakterLap;
	private UzletLap uzletLap;
	private HarcLap harcLap;
	private InventoryLap inventoryLap;
	
	private HorizontalLayout menu;
	private Button harc;
	private Button karakterkrea;
	private Button uzlet;
	private Button karakterlap;
	private Button inventory;
	private Button kilepes;

	// alsó sáv
	private AbsoluteLayout also;
	private VerticalLayout expaction;
	private ActionBar actionbar;
	private ProgressBar expbar;
	private Label kep1;
	private Label kep2;

	// háttér videó
	private Label hatterKep;

	public MainPage() {
		// felső sáv
		party = new CssLayout();

		// középső sáv, technikailag ide tartozik a jobb alul lévő menü
		kozepso = new HorizontalLayout();
		menu = new HorizontalLayout();
		
		harc = new Button("");
		karakterkrea = new Button("");
		uzlet = new Button("");
		karakterlap = new Button("");
		inventory = new Button("");
		kilepes = new Button("");

		// alsó sáv
		also = new AbsoluteLayout();
		expaction = new VerticalLayout();
		kep1 = new Label();
		kep2 = new Label();

		// háttérvideó
		hatterKep = new Label();

	}

	@PostConstruct
	private void postInit() {
		setSizeFull();
		addStyleName("fullscreen-layout");
		addComponents(party, kozepso, also);
		setExpandRatio(party, 1.8f);
		setExpandRatio(kozepso, 7);
		setExpandRatio(also, 1.8f);
		setComponentAlignment(also, Alignment.BOTTOM_CENTER);
		
		ellenfel = new GepiKarakter();
		ellenfel.setNev("Csontváz");
		ellenfel.setArany(1000);
		ellenfel.setElet(500);
		ellenfel.setMaxElet(500);
		ellenfel.setMana(500);
		ellenfel.setMaxMana(500);
		ellenfel.setSzint(10);
		ellenfel.setTamadas(10);
		
		// felső sáv
		party.setStyleName("party");
		party.setSizeFull();


		// középső sáv, technikailag ide tartozik a jobb alul lévő menü
		kozepso.setStyleName("kozep");
		kozepso.setSizeFull();
		kozepso.addComponent(menu);
		kozepso.addComponent(hatterKep);
		menu.addComponents(harc, karakterkrea, uzlet, karakterlap, inventory, kilepes);
		menu.addStyleName("also-menu");
		
		karakterLap = new KarakterLap(MyUI.getCurrent().getUser().getAktivKarakter());
		uzletLap = new UzletLap(presenterMain.findUzlet("Vegyesbolt"));
		harcLap = new HarcLap(MyUI.getCurrent().getUser().getAktivKarakter(), ellenfel);
		inventoryLap = new InventoryLap(MyUI.getCurrent().getUser().getAktivKarakter());
		
		buttozas(harc, "harc-button", "attack","Új harc indítása");
		buttozas(karakterkrea, "karakterkrea-button", "newchar", "Új karakter készítése");
		buttozas(uzlet, "uzlet-button", "shop", "Vásárlás az üzletben");
		buttozas(karakterlap, "karakterlap-button", "char", "Karakter megtekintése");
		buttozas(inventory, "inventory-button", "bag", "Karakter zsákjának tartalma");
		buttozas(kilepes, "kilepes-button", "exit", "Kilépés minden figyelmeztetés nélkül");
		


		// alsó sáv
		int width = Page.getCurrent().getBrowserWindowWidth();
		
		also.setStyleName("also");
		also.addComponent(expaction, "left: "+(width/2-300)+"px; bottom: 0px;");
		also.addComponent(kep1, "left: "+(width/2-448)+"px; bottom: 0px;");
		also.addComponent(kep2, "right: "+(width/2-448)+"px; bottom: 0px;");
		

		expaction.setStyleName("action-exp");
		expaction.setWidth("600px");
		
		expbar = new ProgressBar((float)
				MyUI.getCurrent().getUser().getAktivKarakter().getTp()/
				MyUI.getCurrent().getUser().getAktivKarakter().getKovszintTp());
		expbar.setWidth("100%");
		expbar.addStyleName("exp-bar");
		
		kep1.setHeight("192px");
		kep1.setWidth("192px");
		kep1.setStyleName("actionbar-kep-bal");
		kep2.setHeight("192px");
		kep2.setWidth("192px");
		kep2.setStyleName("actionbar-kep-jobb");

		// háttérkép
		hatterKep.addStyleName("fullscreen-kep-label");
	}

	@Override
	public void enter(ViewChangeEvent event) {
		
		
		
		actionbar = new ActionBar(MyUI.getCurrent().getUser().getAktivKarakter());
		
		actionbar.getAttack().addClickListener(e->{
			ellenfel.setElet(ellenfel.getElet()-10);
			harcLap.feltoltes(MyUI.getCurrent().getUser().getAktivKarakter(), ellenfel);
			if(ellenfel.getElet()<0){
				TextArea area = new TextArea();
				Window window = new Window();
				window.setStyleName("windowok");
				window.setContent(area);
				area.setWidth(100, Unit.PERCENTAGE);
				area.setHeight(100, Unit.PERCENTAGE);
				area.setValue("Gratulálok, sikerült elverned ezt a védtelen pacit.\n"
						+ "Jutalmad ma elmarad. És szégyelld el magad.\n"
						+ "De full frankón .. mégcsak nem is ütött vissza.");
				window.setCaption("Harc a gonosz paci ellen végetért");
				window.setWidth("400px");
				window.setHeight("400px");
				window.setClosable(true);
				window.setDraggable(false);
				window.setResizable(false);
				window.center();
				window.setModal(true);
				MyUI.getCurrent().addWindow(window);
			}
		});
		
		actionbar.getSpell3().addClickListener(e->{
			ellenfel.setElet(ellenfel.getElet()-100);
			harcLap.feltoltes(MyUI.getCurrent().getUser().getAktivKarakter(), ellenfel);
		});
		
		expaction.addComponents(expbar, actionbar);
		expaction.setComponentAlignment(expbar, Alignment.BOTTOM_CENTER);
		expaction.setComponentAlignment(actionbar, Alignment.MIDDLE_CENTER);
		
		//Fenti rész generálása, databázéból
		for (JatekosKarakter karakter : MyUI.getCurrent().getUser().getKarakterek()) {
	
			if(karakter.equals(MyUI.getCurrent().getUser().getAktivKarakter())){
				aktivKarakter=new KarakterUnitFrames(karakter);
				aktivKarakter.addStyleName("aktiv-karakter");
				aktivKarakter.setWidth("340px");
				aktivKarakter.setHeight("125px");
				party.addComponent(aktivKarakter);
			}

			else{
				KarakterUnitFrames kar = new KarakterUnitFrames(karakter);
				party.addComponent(kar);
				kar.addLayoutClickListener(e -> {
					if(e.isDoubleClick()){
						try {
							presenterMain.csere(MyUI.getCurrent().getUser(), karakter);
							MyUI.getCurrent().getNavigator().navigateTo("main_page");
						} catch (Exception e1) {
							Notification.show("Nem lehetséges a karakter kiválasztása.",Type.ERROR_MESSAGE);
							e1.printStackTrace();
						}
					}
				});
			}
		}

		karakterkrea.addClickListener(e -> MyUI.getCurrent().getNavigator().navigateTo("charcreate_screen"));
		
		uzlet.addClickListener(e -> ablakNyitas(uzletLap));
		uzlet.setClickShortcut(KeyCode.S);
		
		uzletLap.getVasarlasGomb().addClickListener(e->{
			int comszam = uzletLap.getSorLayout().getComponentCount();
			for (int i=0; i<comszam; i++){
				FelszerelesButton button = (FelszerelesButton)uzletLap.getSorLayout().getComponent(0);
				Felszereles felsz = button.getFelszereles();
				try {
					presenterMain.vasarlas(felsz, MyUI.getCurrent().getUser().getAktivKarakter());
					uzletLap.getSorLayout().removeComponent(button);
				} catch (Exception e1) {
					Notification.show("Nincs elég aranyad a vásárláshoz.", Type.ERROR_MESSAGE);
					// TODO Auto-generated catch block
					//e1.printStackTrace();
				}
			}
			inventoryLap.feltoltes(MyUI.getCurrent().getUser().getAktivKarakter());
			oltozes();
		});
		
		oltozes();
		
		karakterlap.addClickListener(e -> ablakNyitas(karakterLap));
		karakterlap.setClickShortcut(KeyCode.C);
		
		harc.addClickListener(e -> ablakNyitas(harcLap));
		harc.setClickShortcut(KeyCode.A);
		
		inventory.addClickListener(e -> ablakNyitas(inventoryLap));
		inventory.setClickShortcut(KeyCode.B);
		
		kilepes.addClickListener(this::kilepes);
		
	}
	
	private void oltozes(){
		GridLayout zsak = inventoryLap.getZsak();
		for(int i=0; i<zsak.getColumns(); i++){
			for (int j = 0; j < zsak.getRows(); j++) {
				VerticalLayout layout = (VerticalLayout)zsak.getComponent(i, j);
				if(layout.getComponentCount()==1){
					FelszerelesButton button = (FelszerelesButton)layout.getComponent(0);
					button.addClickListener(e -> {
						try {
							presenterMain.oltoztetes(
									MyUI.getCurrent().getUser(),
									MyUI.getCurrent().getUser().getAktivKarakter(),
									button.getFelszereles());
							karakterLap.feltoltes(MyUI.getCurrent().getUser().getAktivKarakter());
							inventoryLap.feltoltes(MyUI.getCurrent().getUser().getAktivKarakter());
							int index = party.getComponentIndex(aktivKarakter);
							party.removeComponent(aktivKarakter);
							aktivKarakter=new KarakterUnitFrames(MyUI.getCurrent().getUser().getAktivKarakter());
							aktivKarakter.addStyleName("aktiv-karakter");
							aktivKarakter.setWidth("340px");
							aktivKarakter.setHeight("125px");
							party.addComponent(aktivKarakter, index); 
							oltozes();
						} catch (Exception e1) {
							Notification.show("Nem lehetséges a felszerelés felvétele.",Type.ERROR_MESSAGE);
							e1.printStackTrace();
						}
					});
				}
			}
		}
	}
	
	private void kilepes(ClickEvent event) {
		try {
			presenterLogin.kileptet(MyUI.getCurrent().getUser());
			MyUI.getCurrent().setUser(null);
			MyUI.getCurrent().getNavigator().navigateTo("login_screen");
		} catch (Exception e1) {
			//Notification.show("Ilyen felhasználónévvel már regisztráltak.", Type.ERROR_MESSAGE);
		}
	}
	
	private void ablakNyitas(Window ablak){
		Boolean volt = false;
		for (Window window : MyUI.getCurrent().getWindows()) if(window.equals(ablak)) volt = true;
		if(volt) MyUI.getCurrent().removeWindow(ablak);
		else MyUI.getCurrent().addWindow(ablak);
	}
	
	private void buttozas(Button button, String stylename, String kepnev, String desc){
		Styles styles = Page.getCurrent().getStyles();
		button.setStyleName(stylename);
		button.setHeight("60px");
		button.setWidth("40px");
		button.setDescription(desc);
		
	   styles.add(".v-app ." + stylename + " { " 
	        		+ "border: 1px solid grey;"
	        		+ "margin-left:5px;"
	        		+ "margin-right:10px;"
	        		+ "box-shadow: 0px 0px 18px 4px rgba(101,103,128,1);"
	        		+ "background: url(VAADIN/themes/mytheme/pictures/Menu/"+kepnev+".jpg);"
	        		+ "background-size: 100% 100%;"
	        		+ "}");
	}
}
