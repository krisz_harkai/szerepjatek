package szerepjatek.vaadin;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.annotations.Theme;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.Navigator.ComponentContainerViewDisplay;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

import szerepjatek.entities.Felhasznalo;

@Theme("mytheme")
@SpringUI
public class MyUI extends UI {

	private Felhasznalo user;

	private Navigator navigator;

	@Autowired
	private SpringViewProvider viewProvider;

	public Felhasznalo getUser() {
		return user;
	}

	public void setUser(Felhasznalo user) {
		this.user = user;
	}

	public static MyUI getCurrent() {
		return (MyUI) UI.getCurrent();
	}

	@Override
	protected void init(VaadinRequest vaadinRequest) {

		/*
		 * //main layout VerticalLayout layout = new VerticalLayout();
		 * layout.addStyleName("fullscreen-layout");
		 * 
		 * //middle form FormLayout form = new FormLayout();
		 * form.setWidth("450px"); form.setHeight("650px");
		 * form.setStyleName("belepes");
		 * 
		 * TextField nev = new TextField("Név"); nev.setIcon(FontAwesome.USER);
		 * //tf1.setRequired(true); //tf1.addValidator(new NullValidator(
		 * "Must be given", false)); form.addComponent(nev);
		 * nev.setStyleName("kozos");
		 * 
		 * PasswordField jelszo = new PasswordField("jelszó");
		 * jelszo.setIcon(FontAwesome.USER); form.addComponent(jelszo);
		 * jelszo.setStyleName("kozos");
		 * 
		 * Button button = new Button("Bejelentkezés");
		 * form.addComponent(button); button.setStyleName("kozos");
		 * 
		 * //Label label = new Label("World of Nocraft");
		 * //form.addComponent(label); //label.setStyleName("flashing-label");
		 * 
		 * //Videó háttér Label v = new Label(
		 * "<video playsinline autoplay muted loop poster=\"loading.png\" id=\"fullscreen-video\">"
		 * + "<source src=\"world_bg.mp4\" type=\"video/mp4\">" + "</video>",
		 * ContentMode.HTML); v.addStyleName("fullscreen-video-label");
		 * 
		 * layout.addComponent(v); //layout.addComponent(label);
		 * //layout.setComponentAlignment(label, Alignment.MIDDLE_CENTER);
		 * layout.addComponent(form); setContent(layout);
		 */

		final VerticalLayout layout = new VerticalLayout();
		layout.setMargin(true);
		layout.setSpacing(true);
		layout.setSizeFull();
		setContent(layout);

		final ComponentContainerViewDisplay viewDisplay = new ComponentContainerViewDisplay(layout);

		navigator = new Navigator(this, viewDisplay);
		navigator.addProvider(viewProvider);

		navigator.navigateTo("login_screen");
		// navigator.navigateTo("main_page");
		// navigator.navigateTo("charcreate_screen");

		navigator.addViewChangeListener(new ViewChangeListener() {

			@Override
			public boolean beforeViewChange(ViewChangeEvent event) {
				for (Window w : new ArrayList<Window>(getCurrent().getWindows())) {
					getCurrent().removeWindow(w);
				}
				return true;
			}

			@Override
			public void afterViewChange(ViewChangeEvent event) {
			}

		});
	}
}