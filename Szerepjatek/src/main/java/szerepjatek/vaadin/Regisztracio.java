package szerepjatek.vaadin;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import com.vaadin.annotations.Title;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.navigator.ViewProvider;
import com.vaadin.server.ExternalResource;
import com.vaadin.server.Page;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Link;
import com.vaadin.ui.Notification;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification.Type;

import szerepjatek.entities.Felhasznalo;
import szerepjatek.vaadin.presenters.LoginPresenter;
import szerepjatek.vaadin.presenters.RegisztracioPresenter;

@Title("Regisztráció")
@SpringView(name = "regisztracio_screen")
@Scope(value = "prototype", proxyMode = ScopedProxyMode.INTERFACES)
public class Regisztracio extends VerticalLayout implements View {

	private FormLayout form;
	private Label cim;
	private Label hiba;
	private TextField nev;
	private TextField email;
	private TextField felhnev;
	private PasswordField jelszo;
	private Button logButton;
	private Button belepes;
	private Label hatterVideo;
	
	@Autowired
	private RegisztracioPresenter presenter;

	public Regisztracio() {
		setSizeFull();

		form = new FormLayout();
		cim = new Label("Regisztráció");
		hiba = new Label("Hibás valami.");
		nev = new TextField();
		email = new TextField();
		felhnev = new TextField();
		jelszo = new PasswordField();
		belepes = new Button("Regisztrálás");
		logButton = new Button("Már van felhasználói fiókod? Lépj be vele!");
		hatterVideo = new Label("<video playsinline autoplay muted loop poster=\"pictures/loading.png\" id=\"fullscreen-video\">"
				+ "<source src=\"world_bg.mp4\" type=\"video/mp4\">" + "</video>", ContentMode.HTML);
	}

	@PostConstruct
	private void postInit() {
		addStyleName("fullscreen-layout");
		addComponent(hatterVideo);
		addComponent(form);
		
		belepes.addStyleName("buttonee");
		belepes.setWidth("100%");
		
		form.setWidth("500px");
		form.addComponents(cim, hiba, nev, felhnev, jelszo, email, belepes, logButton);
		form.addStyleName("login-form");

		hiba.setVisible(false);

		nev.setInputPrompt("Név");
		nev.setWidth("100%");

		email.setInputPrompt("Email");
		email.setWidth("100%");
		
		felhnev.setInputPrompt("Felhasználónév");
		felhnev.setWidth("100%");

		jelszo.setInputPrompt("jelszó");
		jelszo.setWidth("100%");

		logButton.addStyleName("link");

		hatterVideo.addStyleName("fullscreen-video-label");
	}

	@Override
	public void enter(ViewChangeEvent event) {
		logButton.addClickListener(e -> MyUI.getCurrent().getNavigator().navigateTo("login_screen"));
		belepes.addClickListener(this::belepesEvent);
	}
	
	private void belepesEvent(ClickEvent event) {
		try {
			if (nev.getValue().trim().isEmpty() || jelszo.getValue().trim().isEmpty()
					|| felhnev.getValue().trim().isEmpty() || email.getValue().trim().isEmpty()) {
				createAndShowNotification("Minden mező kitöltése kötelező.", Type.WARNING_MESSAGE);
			} else {
				Felhasznalo user = presenter.regisztral(nev.getValue(), felhnev.getValue(), 
						email.getValue(), jelszo.getValue());
				if (user != null) {
					createAndShowNotification("Sikeresen regisztrált "+felhnev.getValue()+" felhasználónévvel.\nMost már be tud vele jelentkezni.",
							Type.HUMANIZED_MESSAGE);
					MyUI.getCurrent().getNavigator().navigateTo("login_screen");
				}
			}
		} catch (Exception e1) {
			createAndShowNotification("Ezzel a felhasználónévvel már regisztráltak.", Type.ERROR_MESSAGE);
		}
	}
	
	public void createAndShowNotification(String caption, Notification.Type type) {
	    Notification notif = new Notification(caption, type);
	    notif.setPosition(Notification.POSITION_CENTERED_TOP);
	    notif.setDelayMsec(2000);
	    notif.show(Page.getCurrent());
	}
}
