package szerepjatek.vaadin;

import org.springframework.beans.factory.annotation.Autowired;

import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.Notification.Type;

import szerepjatek.entities.Felszereles;
import szerepjatek.entities.Uzlet;
import szerepjatek.vaadin.presenters.MainPagePresenter;

public class UzletLap extends Window {	
	private VerticalLayout windowContent;
	
	private Panel felszPanel;
	private VerticalLayout felszk;
	
	private VerticalLayout also;
	private CssLayout sor;
	private Panel sorPanel;
	private Button vasarlas;
	
	public UzletLap(Uzlet uzlet){
		windowContent = new VerticalLayout();
		felszPanel = new Panel();
		felszk = new VerticalLayout();
		also = new VerticalLayout();
		sorPanel = new Panel();
		sor = new CssLayout();
		vasarlas = new Button("Vásárlás");
		
		windowContent.addComponents(felszPanel, also);
		windowContent.setSizeFull();
		windowContent.setSpacing(true);
		setContent(windowContent);
		
		felszPanel.setSizeFull();
		felszPanel.setContent(felszk);
		
		vasarlas.addStyleName("buttonee");
		vasarlas.setWidth("100%");
		
		for (Felszereles felsz: uzlet.getAruk()) {
			FelszerelesLayout buttone = new FelszerelesLayout(felsz);
			buttone.addLayoutClickListener(e -> {
				FelszerelesButton buttonee = new FelszerelesButton(felsz);
				buttonee.addClickListener(ee -> {
					sor.removeComponent(buttonee);
				});
				sor.addComponent(buttonee);
			});
			felszk.addComponent(buttone);
		}
		
		
		also.setSizeFull();
		also.addComponents(sorPanel, vasarlas);
		sorPanel.setSizeFull();
		sorPanel.setContent(sor);
		
		windowContent.setExpandRatio(felszPanel, 6);
		windowContent.setExpandRatio(also, 4);
		also.setExpandRatio(sorPanel, 7);
		also.setExpandRatio(vasarlas, 3);
		also.setComponentAlignment(vasarlas, Alignment.MIDDLE_CENTER);
		
		addStyleName("windowok");
		setCaption(uzlet.getNev());
		setWidth("500px");
		setHeight("70%");
		setClosable(true);
		setDraggable(true);
		setResizable(false);
		//windo.setResponsive(true);
		center();
	}
	
	public Button getVasarlasGomb(){
		return this.vasarlas;
	}
	
	public CssLayout getSorLayout(){
		return this.sor;
	}
}
