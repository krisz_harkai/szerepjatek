package szerepjatek.vaadin.presenters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import szerepjatek.entities.Felhasznalo;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.entities.Osztaly;
import szerepjatek.services.FelhasznaloSzolgaltatasok;
import szerepjatek.services.exceptions.FelhSzolgException;
import szerepjatek.services.exceptions.InvalidCharException;

@Service
public class KarakterPresenter {
	

	@Autowired
	private FelhasznaloSzolgaltatasok userService;
	
	public JatekosKarakter karakterKreal(Felhasznalo felhasznalo, String nev, String nem,
			String faj, Osztaly osztaly, int eletkor) throws InvalidCharException{
		return userService.karakterKeszites(felhasznalo, nev, nem, faj, osztaly, eletkor);
	}

}
