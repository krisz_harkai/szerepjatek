package szerepjatek.vaadin.presenters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import szerepjatek.entities.Felhasznalo;
import szerepjatek.services.FelhasznaloSzolgaltatasok;
import szerepjatek.services.exceptions.FelhSzolgException;

@Service
public class LoginPresenter {
	

	@Autowired
	private FelhasznaloSzolgaltatasok userService;
	
	public Felhasznalo beleptet(String nev, String jelszo) throws FelhSzolgException{
		return userService.belepes(nev, jelszo);
	}
	
	public Felhasznalo kileptet(Felhasznalo felhasznalo) throws FelhSzolgException{
		return userService.kilepes(felhasznalo);
	}

}
