package szerepjatek.vaadin.presenters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import szerepjatek.entities.Felhasznalo;
import szerepjatek.entities.Felszereles;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.entities.Osztaly;
import szerepjatek.entities.Uzlet;
import szerepjatek.services.FelhasznaloSzolgaltatasok;
import szerepjatek.services.KarakterSzolgaltatasok;
import szerepjatek.services.UzletSzolgaltatasok;
import szerepjatek.services.exceptions.FelhSzolgException;
import szerepjatek.services.exceptions.InvalidCharException;
import szerepjatek.services.exceptions.ShoppingException;

@Service
public class MainPagePresenter {
	
	@Autowired
	private KarakterSzolgaltatasok karakterService;
	
	@Autowired
	private UzletSzolgaltatasok uzletService;
	
	@Autowired
	private FelhasznaloSzolgaltatasok userService;
	
	
	public JatekosKarakter csere(Felhasznalo felhasznalo, JatekosKarakter karakter)
	throws InvalidCharException{
		return userService.karakterValasztas(felhasznalo, karakter);
	}
	
	public JatekosKarakter oltoztetes(Felhasznalo felhasznalo, JatekosKarakter karakter, Felszereles felszereles)
	throws InvalidCharException{
		return karakterService.karakterOltoztetes(felhasznalo, karakter, felszereles);
	}
	
	public JatekosKarakter vetkeztetes(Felhasznalo felhasznalo, JatekosKarakter karakter, Felszereles felszereles)
	throws InvalidCharException{
		return karakterService.karakterVetkeztetes(felhasznalo, karakter, felszereles);
	}
	
	public JatekosKarakter zsakbaPakolas(JatekosKarakter karakter, Felszereles felszereles)
	throws InvalidCharException{
		return karakterService.karakterZsakbaPakolas(karakter, felszereles);
	}
	
	public JatekosKarakter vasarlas(Felszereles felszereles, JatekosKarakter karakter)
	throws ShoppingException{
		return uzletService.vasarlas(felszereles, karakter);
	}
	
	public Uzlet findUzlet(String nev){
		return uzletService.findUzlet(nev);
	}
	
	

}
