package szerepjatek.vaadin.presenters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import szerepjatek.entities.Felhasznalo;
import szerepjatek.services.FelhasznaloSzolgaltatasok;
import szerepjatek.services.exceptions.FelhSzolgException;

@Service
public class RegisztracioPresenter {

	@Autowired
	private FelhasznaloSzolgaltatasok userService;
	
	public Felhasznalo regisztral(String nev, String felhNev, String jelszo, String email) throws FelhSzolgException{
		return userService.regisztralas(nev, felhNev, jelszo, email);
	}

}
