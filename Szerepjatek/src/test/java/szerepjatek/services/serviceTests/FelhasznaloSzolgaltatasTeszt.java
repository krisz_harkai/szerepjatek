package szerepjatek.services.serviceTests;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import szerepjatek.JpaConfig;
import szerepjatek.SzerepjatekConfig;
import szerepjatek.dao.FelhasznaloSzolgDao;
import szerepjatek.dao.impl.JpaFelhasznaloSzolgDao;
import szerepjatek.entities.Felhasznalo;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.entities.Osztaly;
import szerepjatek.services.FelhasznaloSzolgaltatasok;
import szerepjatek.services.exceptions.FelhSzolgException;
import szerepjatek.services.exceptions.InvalidCharException;
import szerepjatek.services.impl.FelhasznaloSzolgaltatasokImpl;

public class FelhasznaloSzolgaltatasTeszt {

	@Mock
	private FelhasznaloSzolgDao fszdao;

	private FelhasznaloSzolgaltatasok felhsz;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		felhsz = new FelhasznaloSzolgaltatasokImpl(fszdao);
	}

	@Test
	public void registerTest() {
		String felhnev = "Lajos";
		Felhasznalo felh = new Felhasznalo();
		Felhasznalo regisztraltFelh = new Felhasznalo();
		felh.setFelhNev(felhnev);

		when(fszdao.create(any(Felhasznalo.class))).thenReturn(felh);

		//Sima regisztrálás
		try {
			regisztraltFelh = felhsz.regisztralas("", felhnev, "", "");
			assertEquals(felhnev, regisztraltFelh.getFelhNev());
		} catch (FelhSzolgException e) {
			e.printStackTrace();
		}
		
		//duplán regisztrál
		when(fszdao.create(eq(regisztraltFelh))).thenReturn(null);
		try {
			regisztraltFelh = felhsz.regisztralas("", felhnev, "", "");
			fail("Hiba: engedett kétszer beregizni ugyanazzal a felhasználónévvel.");
		} catch (FelhSzolgException e) {
			//e.printStackTrace();
		}		
		verify(fszdao, times(2)).create(any(Felhasznalo.class));
				
	}

	@Test
	public void loginTest() {
		String felhnev = "Kálmán";
		Felhasznalo felh = new Felhasznalo();
		felh.setFelhNev(felhnev);

		when(fszdao.findByNameAndPass(eq(felhnev), any(String.class))).thenReturn(felh);

		//belépés
		try {
			Felhasznalo belepettFelh = felhsz.belepes(felhnev, "somepasswd");
			assertEquals(felhnev, belepettFelh.getFelhNev());
		} catch (FelhSzolgException e) {
			e.printStackTrace();
		}
		
		//kilépés
		try {
			Felhasznalo kileptetettFelh = felhsz.kilepes(felh);
			assertEquals(felhnev, kileptetettFelh.getFelhNev());
		} catch (FelhSzolgException e) {
			e.printStackTrace();
		}
			

		verify(fszdao, times(1)).findByNameAndPass(felhnev, "somepasswd");
	}
	
	@Test
	public void characterCreateTest() {
		String nev = "Kálmán";
		JatekosKarakter karakter = new JatekosKarakter();
		karakter.setNev(nev);

		String felhnev = "Kálmán";
		Felhasznalo felh = new Felhasznalo();
		felh.setKarakterek(new ArrayList<>());
		felh.setFelhNev(felhnev);
		
		when(fszdao.read(eq(JatekosKarakter.class), any(String.class))).thenReturn(null);
		when(fszdao.update(any(Felhasznalo.class))).thenReturn(felh);

		try {
			JatekosKarakter letrehozottKarakter = felhsz.karakterKeszites(felh, nev, "", "", new Osztaly(), 10);
			assertEquals(nev, letrehozottKarakter.getNev());
		} catch (InvalidCharException e) {
			e.printStackTrace();
		}	
			

		verify(fszdao, times(1)).read(JatekosKarakter.class, nev);
		verify(fszdao, times(1)).update(felh);
	}
	
/*	@Test
	public void characterDeleteTest() {
		String nev = "Kálmán";
		JatekosKarakter karakter = new JatekosKarakter();
		karakter.setNev(nev);
		
		when(fszdao.read(eq(JatekosKarakter.class), any(String.class))).thenReturn(null);
		when(fszdao.update(any(Felhasznalo.class))).thenReturn(felh);

		try {
			JatekosKarakter letrehozottKarakter = felhsz.karakterKeszites(felh, nev, "", "", new Osztaly(), 10);
			assertEquals(nev, letrehozottKarakter.getNev());
		} catch (InvalidCharException e) {
			e.printStackTrace();
		}	
			

		verify(fszdao, times(1)).read(JatekosKarakter.class, nev);
		verify(fszdao, times(1)).update(felh);
	}*/
}
