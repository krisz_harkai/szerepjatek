/*package szerepjatek.services.serviceTests;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.transaction.annotation.Transactional;

import szerepjatek.JpaConfig;
import szerepjatek.SzerepjatekConfig;
import szerepjatek.entities.Felhasznalo;
import szerepjatek.entities.Felszereles;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.entities.Osztaly;
import szerepjatek.entities.Tipus;
import szerepjatek.services.FelhasznaloDao;
import szerepjatek.services.FelhasznaloSzolgaltatasok;
import szerepjatek.services.FelszerelesDao;
import szerepjatek.services.JatekDao;
import szerepjatek.services.JatekSztoriDao;
import szerepjatek.services.JatekosKarakterDao;
import szerepjatek.services.KarakterSzolgaltatasok;
import szerepjatek.services.exceptions.InvalidCharException;
import szerepjatek.services.impl.KarakterSzolgaltatasokImpl;

@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SzerepjatekConfig.class, JpaConfig.class }, loader = AnnotationConfigContextLoader.class)
public class KarakterSzolgaltatasokTeszt {
	private static Felhasznalo felh1;
	private static Felhasznalo felh2;
	private static Felszereles felszereles1;
	private static Felszereles felszereles2;
	
	@Autowired
	private KarakterSzolgaltatasok karsz;
	@PersistenceContext
	private EntityManager entityManager;

	@BeforeClass
	public static void beforeBeforeClass() {
		felh1 = felhdao.createFelhasznalo("Krisz", "krisz", "2345", "krisz@gmail.com");
		felh2 = felhdao.createFelhasznalo("Pali", "pali", "2345", "pali@gmail.com");

		felszereles1 = felszdao.createFelszereles("Paplan", "egy mezei paplan", Tipus.PALAST, 0, 10, 10, 10, 0);
		felszereles2 = felszdao.createFelszereles("Paplanv2", "egy mezei paplanv2", Tipus.PALAST, 20, 0, 30, 30, 0);
	}

	@Before
	public void beforeClass() {
		JatekosKarakterDao jtkdao = new DummyJatekosKarakterDao();
		FelhasznaloDao felhdao = new DummyFelhasznaloDao();
		JatekDao jatdao = new DummyJatekDao();
		JatekSztoriDao jtkszdao = new DummyJatekSztoriDao();
		karsz = new KarakterSzolgaltatasokImpl(jtkdao, felhdao, jatdao, jtkszdao);

	}

	

	@Test
	public void ertekekMegvaltozasaTeszt() {
		JatekosKarakter karakter1 = new JatekosKarakter();
		JatekosKarakter karakter2 = new JatekosKarakter();
		try {
			karakter1 = karsz.karakterKeszites(felh1, "Vadlol", "male", "human", new Osztaly(), 55);
			karakter2 = karsz.karakterKeszites(felh2, "Fadlol", "male", "human", new Osztaly(), 45);
			fail("Hiba: engedte ugyan azzal a n�vvel l�trehozni egy m�sik k�r�ktert.");
		} catch (InvalidCharException e2) {
			// e2.printStackTrace();
		}

		try {
			karsz.karakterValasztas(felh1, karakter1);
			karsz.karakterZsakbaPakolas(karakter1, felszereles1);
			karsz.karakterOltoztetes(felh1, karakter1, felszereles1);
			assertEquals("Nem egyeznek az �rt�kek", 20, karakter1.getTamadas());

			karsz.karakterZsakbaPakolas(karakter1, felszereles2);
			assertEquals("Nem egyeznek az �rt�kek", 20, karakter1.getTamadas());

			karsz.karakterOltoztetes(felh1, karakter1, felszereles2);
			assertEquals("Nem egyeznek az �rt�kek", 10, karakter1.getTamadas());
			// zs�kban van a felszereles1, a felszereles2 meg rajta
		} catch (InvalidCharException e) {
			// e.printStackTrace();
		}

		try {
			karsz.karakterZsakbolTorles(karakter1, felszereles2);
			fail("Hiba: megengedett olyan cuccot t�r�lni, ami nem volt a zs�kj�ban.");
		} catch (InvalidCharException e1) {
			// e1.printStackTrace();
		}

		try {
			karsz.karakterZsakbolTorles(karakter1, felszereles2);
			karsz.karakterOltoztetes(felh1, karakter1, felszereles1);
			fail("Hiba: t�r�lt felszerel�st megengedett felvenni.");
		} catch (InvalidCharException e) {
			// e.printStackTrace();
		}
	}

}
*/