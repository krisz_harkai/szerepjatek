package szerepjatek.services.serviceTests;

import org.junit.Ignore;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.Persistence;

@Ignore
public class SchemaCreateIntegrationTest {

    @Test
    public void databaseDllScriptH2Test() {
        Map<String, Object> properties = new HashMap<>();

        properties.put("javax.persistence.jdbc.driver", "org.h2.Driver");
        properties.put("javax.persistence.jdbc.url", "jdbc:h2:mem:testdb");
        properties.put("javax.persistence.jdbc.user", "sa");
        properties.put("javax.persistence.jdbc.password", "");
        properties.put("javax.persistence.schema-generation.scripts.create-target", "target/create-h2.sql");
        properties.put("javax.persistence.schema-generation.scripts.drop-target", "target/drop-h2.sql");

        Persistence.generateSchema("default", properties);
    }

}