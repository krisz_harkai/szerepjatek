/*package szerepjatek.services.serviceTests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import szerepjatek.entities.Felhasznalo;
import szerepjatek.entities.Felszereles;
import szerepjatek.entities.JatekosKarakter;
import szerepjatek.entities.Osztaly;
import szerepjatek.entities.Tipus;
import szerepjatek.services.FelhasznaloSzolgaltatasok;
import szerepjatek.services.FelszerelesDao;
import szerepjatek.services.JatekosKarakterDao;
import szerepjatek.services.UzletSzolgaltatasok;
import szerepjatek.services.exceptions.ShoppingException;
import szerepjatek.services.impl.UzletSzolgaltatasokImpl;
import szerepjatek.services.serviceTests.dummy.DummyFelszerelesDao;
import szerepjatek.services.serviceTests.dummy.DummyJatekosKarakterDao;

public class UzletSzolgaltatasokTeszt {

	private static JatekosKarakter karakter1;
	private static JatekosKarakter karakter2;
	private static Felszereles felszereles1;
	private static Felszereles felszereles2;
	private UzletSzolgaltatasok uzletsz;
	
	@BeforeClass
	public static void beforeBeforeClass(){
		FelszerelesDao felszdao = new DummyFelszerelesDao();
		JatekosKarakterDao jatkdao = new DummyJatekosKarakterDao();
		
		karakter1 = jatkdao.createJatekosKarakter("Vadlol", "male", "human", new Osztaly(), 55);
		karakter2 = jatkdao.createJatekosKarakter("Fadlol", "male", "human", new Osztaly(), 40);
		
		felszereles1 =  felszdao.createFelszereles(
				"Paplan", "egy mezei paplan", Tipus.PALAST, 0, 0, 0, 0, 0);
		felszereles2 =  felszdao.createFelszereles(
				"Paplanv2", "egy mezei paplanv2", Tipus.PALAST, 20, 0, 0, 0, 0);
	}
	
	@Before
	public void beforeClass(){
		FelszerelesDao felszdao = new DummyFelszerelesDao();
		JatekosKarakterDao jatkdao = new DummyJatekosKarakterDao();
		
		uzletsz= new UzletSzolgaltatasokImpl(felszdao, jatkdao);	
	}
	
	@Test
	public void testVasarlas() {
		try {
			uzletsz.vasarlas(felszereles1, karakter1);
		} catch (ShoppingException e) {
			//e.printStackTrace();
		}
		
		try {
			uzletsz.vasarlas(felszereles2, karakter1);
		} catch (ShoppingException e) {
			//e.printStackTrace();
		}
	}

	@Test
	public void testEladas() {
		try {
			uzletsz.eladas(felszereles1, karakter1);
		} catch (ShoppingException e) {
			//e.printStackTrace();
		}
		
		try {
			uzletsz.eladas(felszereles2, karakter1);
		} catch (ShoppingException e) {
			//e.printStackTrace();
		}
	}
	

}
*/