INSERT INTO public.osztaly(nev)
    VALUES ('Paladin'),
	('M�gus'),
	('B�rd');

INSERT INTO public.kepesseg(nev, gyogyitas, koltseg, sebzes)
    VALUES ('Crusader Strike', 0, 20, 100),
	('Hammer of Wrath', 0, 100, 300),
	('Lay on Hands', 250, 500, 0),
	('T�zgoly�', 0, 20, 100),
	('Goly� Goly�', 0, 100, 500),
	('Wrapper', 0, 20, 5),
	('Mib�', 250, 10, 0),
	('Lady Ann', 0, 10, 500),
	('L�k�s', 0, 20, 20);
	
INSERT INTO public.osztaly_kepesseg(osztaly_nev, kepessegek_nev)
    VALUES ('Paladin', 'Crusader Strike'),
	('Paladin', 'Hammer of Wrath'),
	('Paladin', 'Lay on Hands'),
	('M�gus', 'T�zgoly�'),
	('M�gus', 'Goly� Goly�'),
	('M�gus', 'Wrapper'),
	('B�rd', 'L�k�s'),
	('B�rd', 'Mib�'),
	('B�rd', 'Lady Ann');

INSERT INTO public.felszereles(nev, ar, elet, leiras, mana, ritkasag, tamadas, tipus, vedekezes)
    VALUES ('Fakard', 2, 5, 'Egy fakard, semmi t�bb.', 5, 'COMMON', 10, 'MAINHAND', 0),
	('Fasisak', 2, 10, 'Egy f�b�l k�sz�lt sisak, semmi t�bb.', 0, 'COMMON', 0, 'FEJES', 5),
	('Fanadr�g', 2, 10, 'Egy f�b�l k�sz�lt nadr�g. Igen, van ilyen.', 0, 'COMMON', 0, 'NADRAG', 10),
	('Trubad�rpajzs', 2, 10, 'Valaha a leghatalmasabbak is ezt hordt�k. Nagy n�t�sok voltak.', 0, 'RARE', 0, 'OFFHAND', 5),
	('Pizsama', 2, 10, 'Egy j� pizsama, sose rossz. Tarcsa a mond�s.', 0, 'UNCOMMON', 0, 'NADRAG', 5),
	('Bakancs', 2, 10, 'Egy bakello, nincs benne semmi �rdekes', 0, 'RARE', 0, 'CIPO', 5),
	('Sityak', 2, 200, 'Ebben lehet csak igaz�n csajozni!', 10, 'EPIC', 10, 'FEJES', 10),
	('V�llv�rte', 2, 200, 'Nem is �rte v�ll, hihi', 10, 'EPIC', 20, 'VALLAS', 10),
	('Ac�lpapucs', 2, 100, 'Nem csak ny�rra!', 10, 'EPIC', 10, 'CIPO', 10),
	('G�rb�', 2, 300, 'Kicsit meleg, kicsit .. garb�', 10, 'EPIC', 30, 'FELSO', 30),
	('Keszty�', 2, 300, 'Pihepuha keszty�', 10, 'EPIC', 30, 'KESZTYU', 30),
	('Nadr�ghamondom', 2, 200, 'Ezt is hordja Korda Gyuri', 20, 'EPIC', 20, 'NADRAG', 20),
	('Gy�r�', 2, 50, 'Ezt hordja Korda Gyuri', 5, 'EPIC', 5, 'GYURU1', 5),
	('Gy�r�cske', 2, 50, 'Ezt m�r nem hordja Korda Gyuri', 5, 'EPIC', 5, 'GYURU2', 5),
	('Medalij�n', 2, 50, 'Kezdek kifogyni frapp�ns sz�vegekb�l..', 5, 'EPIC', 5, 'NYAKLANC', 5),
	('�v', 2, 50, 'egy �v', 5, 'EPIC', 5, 'OV', 5),
	('Batmank�p', 2, 100, 'batman valaha ezt viselte', 10, 'EPIC', 10, 'PALAST', 10),
	('Bety�r�j', 2, 200, 'Nagyon bety�ros, m�g a nev�ben is benne van', 200, 'EPIC', 200, 'RANGED', 200),
	('Ac�lpajzs', 2, 500, 'Hatalmas ac�lb� k�sz�lt pajzs', 500, 'LEGENDARY', 500, 'OFFHAND', 1000),
	('Ac�lkard', 2, 500, 'Ac�lsz���ve', 500, 'LEGENDARY', 1000, 'MAINHAND', 500);
	
	
INSERT INTO public.uzlet(nev)
    VALUES ('Vegyesbolt');
	
INSERT INTO public.uzlet_felszereles(uzlet_nev, aruk_nev)
    VALUES ('Vegyesbolt', 'Fakard'),
	('Vegyesbolt', 'Fasisak'),
	('Vegyesbolt', 'Fanadr�g'),
	('Vegyesbolt', 'Trubad�rpajzs'),
	('Vegyesbolt', 'Pizsama'),
	('Vegyesbolt', 'Bakancs'),
	('Vegyesbolt', 'Sityak'),
	('Vegyesbolt', 'V�llv�rte'),
	('Vegyesbolt', 'Ac�lpapucs'),
	('Vegyesbolt', 'G�rb�'),
	('Vegyesbolt', 'Nadr�ghamondom'),
	('Vegyesbolt', 'Gy�r�'),
	('Vegyesbolt', 'Gy�r�cske'),
	('Vegyesbolt', 'Medalij�n'),
	('Vegyesbolt', '�v'),
	('Vegyesbolt', 'Batmank�p'),
	('Vegyesbolt', 'Bety�r�j'),
	('Vegyesbolt', 'Ac�lpajzs'),
	('Vegyesbolt', 'Ac�lkard');
	
/*INSERT INTO public.gepikarakter(nev, arany, elet, mana, maxelet, maxmana, szint, tamadas, vedekezes,halalszoveg, kezdoszoveg, tamadasszoveg)
		VALUES ("Paci", 100, 100, 100, 100, 100, 10, 10, 10,"Rawrgh", "Abgduuurrr", "Nia Hao Kai-Lan"),
		("Csonti", 100, 100, 100, 100, 100, 10, 10, 10,"Rawrgh", "Abgduuurrr", "Nia Hao Kai-Lan"),
		("Kebab�rus", 100, 100, 100, 100, 100, 10, 10, 10,"Rawrgh", "Abgduuurrr", "Nia Hao Kai-Lan"),
		("Mark", 100, 100, 100, 100, 100, 10, 10, 10,"Rawrgh", "Abgduuurrr", "Nia Hao Kai-Lan"),
		("�l�halott", 100, 100, 100, 100, 100, 10, 10, 10,"Rawrgh", "Abgduuurrr", "Nia Hao Kai-Lan"),
		("G�sp�rfiv�rek", 100, 100, 100, 100, 100, 10, 10, 10,"J�vanmore, j� szakacs lesz bel�led is", "Mib�?", "Mink lisztb�, sz�dab...");
		
INSERT INTO public.jateksztori(id, nev, tortenet, gepiellenfel_nev)
    VALUES (0, "Erd�", "�ppen s�t�lt�l, amikor pont a G�sp�rfiv�rek j�ttek.", "G�sp�rfiv�rek"),
	(1, "R�t", "�ppen s�t�lt�l, amikor pont a G�sp�rfiv�rek j�ttek.", "G�sp�rfiv�rek"),
	(2, "Puszta", "�ppen s�t�lt�l, amikor pont a G�sp�rfiv�rek j�ttek.", "G�sp�rfiv�rek"),
	(3, "Valahol", "�ppen s�t�lt�l, amikor pont a G�sp�rfiv�rek j�ttek .", "G�sp�rfiv�rek"),
	(4, "V�z alatt", "�ppen s�t�lt�l, amikor pont a G�sp�rfiv�rek j�ttek", "G�sp�rfiv�rek"),
	(5, "Nadr�gban", "�ppen s�t�lt�l, amikor hopp, pont a G�sp�rfiv�rek j�ttek veled szembe.", "G�sp�rfiv�rek");*/